﻿using CardGamePrototype.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Players
{
    public class CardPicker
    {
        private List<CardObject> cardOptions;
        public event EventHandler<CardPickedEventArgs> OnCardPickedEvent;

        public CardPicker()
        {
            cardOptions = new List<CardObject>();
        }

        public bool PickFromField { get; set; }
        public void SetCardOptions(CardObject[] cardList)
        {
            cardOptions = new List<CardObject>(cardList);
        }
        public void OnCardInHandClicked(CardObject card)
        {
            if (cardOptions.Contains(card))
            {
                CardPickedEventArgs args = new CardPickedEventArgs { PickedCard = card };
                OnCardPickedEvent(this, args);
            }
        }
        public class CardPickedEventArgs : EventArgs
        {
            public CardObject PickedCard { get; set; }
        }
    }
}
