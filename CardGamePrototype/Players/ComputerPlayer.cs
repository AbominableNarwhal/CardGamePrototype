﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardGamePrototype.Cards;

namespace CardGamePrototype.Players
{
    public class ComputerPlayer : Player
    {

        protected override IQueryAnswer GetQueryAnswer_Child()
        {
            CombatQueryAnswer answer = new CombatQueryAnswer();
            answer.Type = QueryAnswerType.PlayPowerCard;
            return answer;
        }
    }
}
