﻿using CardGamePrototype.Cards;
using CardGamePrototype.GameBoardTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Players
{
    public interface IInputState
    {
        void OnCardInHandClicked(CardObject card);
        void OnActiveFighterClicked(FighterCard card);
        void OnSupportFighterClicked(FighterCard card);
        void OnAttachedAblilityCardClicked(AbilityCard card);
        void OnEndTurnSignal();
        void ActivateAbilitySignal(Ability ability);
        void ResetState();
    }
    public class CombatInputState : IInputState
    {
        private event EventHandler<CombatInputEventArgs> OnCombatInputEvent;
        private CombatInputEventArgs args;
        private CardObject lastClickedCard;
        //Does this really need an IPlayer object?
        public CombatInputState(IQuery query, IPlayer player, GameBoard gameBoard, EventHandler<CombatInputEventArgs> handler)
        {
            OnCombatInputEvent += handler;
            ResetState();
        }
        public void ResetState()
        {
            args = new CombatInputEventArgs();
            lastClickedCard = null;
        }
        public void OnCardInHandClicked(CardObject card)
        {
            lastClickedCard = card;
            if (card.Type == CardObject.CardType.Power)
            {
                args.Answer.Type = QueryAnswerType.PlayPowerCard;
                args.Answer.Card = card;
                OnCombatInputEvent(this, args);
            }
            else if (card.Type == CardObject.CardType.Ability)
            {
                args.Answer.Type = QueryAnswerType.AttachAbilityCard;
                args.Answer.Card = card;
            }
            else if (card.Type == CardObject.CardType.Aura)
            {
                args.Answer.Type = QueryAnswerType.PlayAura;
                args.Answer.Card = card;
                OnCombatInputEvent(this, args);
            }
            else if (card.Type == CardObject.CardType.Equipment)
            {
                args.Answer.Type = QueryAnswerType.AttachEquipmentCard;
                args.Answer.Card = card;
            }
            else if (card.Type == CardObject.CardType.Item)
            {
                args.Answer.Type = QueryAnswerType.PlayItemCard;
                args.Answer.Card = card;
                OnCombatInputEvent(this, args);
            }
        }
        public void OnActiveFighterClicked(FighterCard card)
        {
            FighterSelected(card);
        }
        public void OnSupportFighterClicked(FighterCard card)
        {
            FighterSelected(card);
        }
        private void FighterSelected(FighterCard card)
        {
            lastClickedCard = card;
            if (args.Answer.Type == QueryAnswerType.AttachAbilityCard ||
                args.Answer.Type == QueryAnswerType.AttachEquipmentCard)
            {
                args.Answer.TargetFighter = card;
                OnCombatInputEvent(this, args);
            }
        }
        public void OnAttachedAblilityCardClicked(AbilityCard card)
        {
            if (lastClickedCard == card)
            {
                args.Answer.Type = QueryAnswerType.ActivateAbility;
                args.Answer.AbilityToActivate = card.Ability;
                OnCombatInputEvent(this, args);
            }
            lastClickedCard = card;
        }

        public void OnEndTurnSignal()
        {
            args.Answer.Type = QueryAnswerType.None;
            OnCombatInputEvent(this, args);
        }
        public void ActivateAbilitySignal(Ability ability)
        {
            args.Answer.Type = QueryAnswerType.ActivateAbility;
            args.Answer.AbilityToActivate = ability;
            OnCombatInputEvent(this, args);
        }
        public class CombatInputEventArgs : EventArgs
        {
            public CombatInputEventArgs()
            {
                Answer = new CombatQueryAnswer();
            }
            public CombatQueryAnswer Answer { get; set; }
        }
    }
}
