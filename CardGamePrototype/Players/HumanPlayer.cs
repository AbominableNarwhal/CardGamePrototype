﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardGamePrototype.Cards;
using static CardGamePrototype.GameRunner;

namespace CardGamePrototype.Players
{
    public class HumanPlayer : Player
    {
        private IInputState inputState;
        public HumanPlayer()
        {
            DeckName = "Deck1";
        }

        CombatQueryAnswer currentAnswer = null;
        protected override IQueryAnswer GetQueryAnswer_Child()
        {
            if (currentAnswer != null)
            {
                CombatQueryAnswer temp = currentAnswer;
                currentAnswer = null;
                return temp;
            }
            return null;
        }
        private void OnCombatInput(object sender, CombatInputState.CombatInputEventArgs e)
        {
            currentAnswer = e.Answer;
        }
        protected override void OnQuerySubmitted_Child(IQuery query, GameRunnerData gameData)
        {
            inputState = new CombatInputState(query, this, gameData.gameBoardController.gameBoard, OnCombatInput);
        }
        public override void OnCardInHandClicked(CardObject card)
        {
            inputState.OnCardInHandClicked(card);
        }
        public override void OnActiveFighterClicked(FighterCard card)
        {
            inputState.OnActiveFighterClicked(card);
        }
        public override void OnSupportFighterClicked(FighterCard card)
        {
            inputState.OnSupportFighterClicked(card);
        }
        public override void OnAttachedAblilityCardClicked(AbilityCard card)
        {
            inputState.OnAttachedAblilityCardClicked(card);
        }
        public override void OnEndTurnSignal()
        {
            inputState.OnEndTurnSignal();
        }
        public override void ActivateAbilitySignal(Ability ability)
        {
            inputState.ActivateAbilitySignal(ability);
        }
    }
}
