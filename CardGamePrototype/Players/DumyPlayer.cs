﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CardGamePrototype.GameRunner;

namespace CardGamePrototype.Players
{
    public class DumyPlayer : Player
    {
        public DumyPlayer()
        {
            DeckName = "Deck1";
        }
        protected override IQueryAnswer GetQueryAnswer_Child()
        {
            if (currentQuery.Type == QueryType.ChooseCombat)
            {
                CombatQueryAnswer answer = new CombatQueryAnswer() { Type = QueryAnswerType.None };
                return answer;
            }
            return null;
        }
    }
}
