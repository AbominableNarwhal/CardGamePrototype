﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardGamePrototype.Cards;
using static CardGamePrototype.GameRunner;

namespace CardGamePrototype.Players
{
    public interface IPlayer
    {
        string DeckName { get; set; }
        void OnQuerySubmitted(IQuery query, GameRunnerData gameData);
        IQueryAnswer GetQueryAnswer();
        void OnCardInHandClicked(CardObject card);
        void OnActiveFighterClicked(FighterCard card);
        void OnSupportFighterClicked(FighterCard card);
        void OnAttachedAblilityCardClicked(AbilityCard card);
        void OnEndTurnSignal();
        void ActivateAbilitySignal(Ability ability);
    }
    public abstract class Player : IPlayer
    {
        public string DeckName { get; set; }
        protected IQuery currentQuery = null;

        public void OnQuerySubmitted(IQuery query, GameRunnerData gameData)
        {
            currentQuery = query;
            OnQuerySubmitted_Child(query, gameData);
        }

        public IQueryAnswer GetQueryAnswer()
        {
            if (currentQuery != null)
            { 
                IQueryAnswer answer = GetQueryAnswer_Child();
                if (answer != null)
                {
                    currentQuery = null;
                }
                return answer;
            }
            return null;
        }
        protected virtual void OnQuerySubmitted_Child(IQuery query, GameRunnerData gameData) { }
        protected virtual IQueryAnswer GetQueryAnswer_Child() { return null; }
        public virtual void OnCardInHandClicked(CardObject card) { }
        public virtual void OnActiveFighterClicked(FighterCard card) { }
        public virtual void OnSupportFighterClicked(FighterCard card) { }

        public virtual void OnAttachedAblilityCardClicked(AbilityCard card) { }

        public virtual void OnEndTurnSignal()
        {
        }
        public virtual void ActivateAbilitySignal(Ability ability)
        { }
    }

}
