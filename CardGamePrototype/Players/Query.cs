﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardGamePrototype.Cards;
using static CardGamePrototype.GameRunner;

namespace CardGamePrototype.Players
{
    public enum QueryType { ChooseCombat, DefensiveResponce };
    public enum QueryAnswerType { None, PlayPowerCard, PlayItemCard, AttachAbilityCard, ActivateAbility, PlayAura, AttachEquipmentCard };
    public interface IQuery
    {
        QueryType Type { get; }
        bool IsValidAnswer(GameRunnerData gameData, IQueryAnswer answer);
    }

    public interface IQueryAnswer
    {
        QueryAnswerType Type { get; set; }
    }

    public class CombatQuery : IQuery
    {
        public QueryType Type
        {
            get
            {
                return QueryType.ChooseCombat;
            }
        }
        public bool IsValidAnswer(GameRunnerData gameData, IQueryAnswer answer)
        {
            return true;
        }
    }
    public class CombatQueryAnswer : IQueryAnswer
    {
        public QueryAnswerType Type { get; set; }
        public CardObject Card { get; set; }
        public FighterCard TargetFighter { get; set; }
        public Ability AbilityToActivate { get; set; }
    }

    public class DefensiveResponceQuery : IQuery
    {
        public QueryType Type
        {
            get
            {
                return QueryType.DefensiveResponce;
            }
        }

        public bool IsValidAnswer(GameRunnerData gameData, IQueryAnswer answer)
        {
            return true;
        }
    }
}
