﻿using CardGamePrototype.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Cards
{
    public class EquipmentCard : CardObject
    {
        public String CardEffectName { get; private set; }
        public String Kind { get; private set; }
        public EquipmentCard(String _Kind, IPlayer _owner = null) : base(_owner)
        {
            Kind = _Kind;
            Type = CardType.Equipment;
        }
    }
}
