﻿using CardGamePrototype.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Cards
{
    public class PowerCard : CardObject
    {
        public long OffensiveCost { get; private set; }
        public long DefensiveCost { get; private set; }
        public long AnyAuraCost { get; private set; }
        public string CardEffectName { get; private set; }
        public ICardEffect PowerCardEffect { get; set; }
        public PowerCard(long _OffensiveCost, long _DefensiveCost, long _AnyAuraCost,
            IPlayer _owner = null, ICardEffect cardEffect = null) : base(_owner)
        {
            OffensiveCost = _OffensiveCost;
            DefensiveCost = _DefensiveCost;
            AnyAuraCost = _AnyAuraCost;
            PowerCardEffect = cardEffect;
            PowerCardEffect?.SetCardUser(this);
            Type = CardType.Power;
        }
    }
}
