﻿using CardGamePrototype.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Cards
{
    public class AuraCard : CardObject
    {
        public bool IsTapped { get; set; }
        public AuraCard(IPlayer _owner = null) : base(_owner)
        {
            Type = CardType.Aura;
        }
    }
}
