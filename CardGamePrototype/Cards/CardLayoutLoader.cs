﻿using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Cards
{
    public static class CardLayoutLoader
    {
        private class FighterCardLoader : DefaultMapLoader
        {
            public override IObjectGroup NewObjectGroup(string name, long id, float x, float y)
            {
                return base.NewObjectGroup(name, id, x, y);
            }
            public override void NewObjectData(IObjectGroup group, string name, float x, float y, float width, float height, float rotation, Texture2D texture, TextData? textData, Dictionary<string, string> properties)
            {
                base.NewObjectData(group, name, x, y, width, height, rotation, texture, textData, properties);
            }
        }

        public static GameObject LoadFighterCardLayout(string fileName)
        {
            FighterCardLoader loader = new FighterCardLoader();
            GameWorldMap map = WorldMapLoader.LoadMap(fileName, loader);
            return (GameObject)map.Objects[0];
        }
    }
}
