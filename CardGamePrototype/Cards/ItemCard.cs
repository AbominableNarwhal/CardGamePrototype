﻿using CardGamePrototype.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Cards
{
    public class ItemCard : CardObject
    {
        public string CardEffectName { get; private set; }
        public ItemCard(IPlayer _owner = null) : base(_owner)
        {
            Type = CardType.Item;
        }
    }
}
