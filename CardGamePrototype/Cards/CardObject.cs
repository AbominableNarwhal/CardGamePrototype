﻿using CardGamePrototype.GameBoardTools;
using CardGamePrototype.Players;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using System;

namespace CardGamePrototype.Cards
{
    public class CardObject : GameObject
    {
        public enum CardType { Power, Fighter, Item, Aura, Equipment, Ability }
        public CardObject(IPlayer _owner = null)
        {
            owner = _owner;
            renderComponent = new SpriteRenderComponent();
            AddComponent(renderComponent);
            Bounds = new RotatedRectangle(Transforms, 0, 0, 0, 0);
        }

        private readonly SpriteRenderComponent renderComponent;
        private RotatedRectangle bounds;
        private bool faceUp = true;
        private Texture2D cardImage;
        private Texture2D cardBack;
        public Texture2D CardImage
        {
            get
            { return cardImage; }
            set
            {
                cardImage = value;
                UpdateSprite();
            }
        }
        public Texture2D CardBack
        {
            get { return cardBack; }
            set
            {
                cardBack = value;
                UpdateSprite();
            }
        }
        public IPlayer owner;
        public string CardName { get; set; }
        public CardType Type {get; set;}
        public string CardTypeStr { get; set; }
        
        public RotatedRectangle Bounds
        {
            get
            {
                bounds.Width = Width;
                bounds.Height = Height;
                return bounds;
            }
            private set
            {
                bounds = value;
            }
        }
        public bool IsVisable {
            get
            {
                return renderComponent.Visable;
            }
            set
            {
                renderComponent.Visable = value;
            }
        }
        private void UpdateSprite()
        {
            renderComponent.SpriteImage.Texture = faceUp ? CardImage : CardBack;
        }
        public bool FaceUp
        {
            get
            {
                return faceUp;
            }
            set
            {
                faceUp = value;
                UpdateSprite();
            }
        }
        public float Rotation
        {
            get { return Transforms.Rotation; }
            set { Transforms.Rotation = value; }
        }
        public float Scale
        {
            get { return Transforms.Scale.X; }
            set { Transforms.Scale = new Vector3(value, value, 0); }
        }
        public virtual float Width
        {
            get { return renderComponent.SpriteImage.Width; }
        }
        public virtual float Height
        {
            get { return renderComponent.SpriteImage.Height; }
        }
        public void SetLayout(CardLayout cardLayout)
        {
            Transforms.Position = new Vector3(cardLayout.Position, 0);
            Rotation = cardLayout.Rotation;
            Scale = cardLayout.Scale;
            IsVisable = cardLayout.IsVisable;
            FaceUp = cardLayout.FaceUp;
        }
        public Texture2D Texture
        {
            get { return renderComponent.SpriteImage.Texture; }
            set { renderComponent.SpriteImage.Texture = value; }
        }
        public int RenderIndex
        {
            get { return renderComponent.Index; }
        }
        
        public void LoadImage(string filename)
        {
            CardImage = MysterionEngineGame.Game.Content.Load<Texture2D>(filename);
        }
  
        public bool ContainsPoint(int x, int y)
        {
            return Bounds.ContainsPoint(new Vector2(x, y));
        }
    }
}
