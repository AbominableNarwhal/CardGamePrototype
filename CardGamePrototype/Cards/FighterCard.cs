﻿using CardGamePrototype.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Cards
{
    public class FighterCard : CardObject
    {
        private List<EquipmentCard> equipmentCards;
        private List<AbilityCard> abilityCards;
        private List<Ability> abilities;

        private readonly FighterStat magic;
        private FighterStat getMagic;
        private FighterStat setMagic;

        private readonly FighterStat tech;
        private FighterStat getTech;
        private FighterStat setTech;

        private readonly FighterStat strength;
        private FighterStat getStrength;
        private FighterStat setStrength;
        public FighterCard(long _HP, long _Magic, long _Tech, long _Strength, long _EquipmentSlots, long _AbilitySlots,
            string _Class, IPlayer _owner = null) : base(_owner)
        {
            abilityCards = new List<AbilityCard>();
            abilities = new List<Ability>();
            equipmentCards = new List<EquipmentCard>();
            HP = _HP;
            magic = new FighterStat((int)_Magic);
            getMagic = setMagic = magic;
            tech = new FighterStat((int)_Tech);
            getTech = setTech = tech;
            strength = new FighterStat((int)_Strength);
            getStrength = setStrength = strength;
            EquipmentSlots = _EquipmentSlots;
            AbilitySlots = _AbilitySlots;
            FighterClass = _Class;
            Type = CardType.Fighter;
        }
        public List<Ability> Abilities { get { return abilities; } }
        public long HP { get; set; }
        public int Magic
        {
            get { return getMagic.Stat; }
            set { setMagic.Stat = value; }
        }
        public void GetMagicRefSet(FighterStat value)
        {
            getMagic = value;
        }
        public FighterStat GetMagicRefGet()
        {
            return getMagic;
        }

        public int Tech
        {
            get { return getTech.Stat; }
            set { setTech.Stat = value; }
        }
        public void GetTechRefSet(FighterStat value)
        {
            getTech = value;
        }
        public FighterStat GetTechRefGet()
        {
            return getTech;
        }

        public int Strength
        {
            get { return getStrength.Stat; }
            set { setStrength.Stat = value; }
        }
        public void GetStrengthRefSet(FighterStat value)
        {
            getStrength = value;
        }
        public FighterStat GetStrengthRefGet()
        {
            return getStrength;
        }

        public long EquipmentSlots { get; private set; }
        public long AbilitySlots { get; private set; }
        public string FighterClass { get; private set; }
        public void AttachAbilityCard(AbilityCard card)
        {
            card.AttachedFighter = this;
            abilityCards.Add(card);
            abilities.Add(card.Ability);
        }
        public void AttachEquipmentCard(EquipmentCard card)
        {
            equipmentCards.Add(card);
        }
        public void AddAbility(Ability ability)
        {
            abilities.Add(ability);
        }
    }
    public class FighterStat
    {
        public FighterStat() { }
        public FighterStat(int value) { Stat = value; }
        public int Stat { get; set; }
    }
}
