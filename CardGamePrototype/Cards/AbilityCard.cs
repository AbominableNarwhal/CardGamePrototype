﻿using CardGamePrototype.Players;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.Cards
{
    public class AbilityCard : CardObject
    {
        public Ability Ability { get; set; }
        public FighterCard AttachedFighter { get; set; }
        public AbilityCard(IPlayer _owner = null) : base(_owner)
        {
            Type = CardType.Ability;
        }
        public void SetAbilityStats(long damage, long magic, long tech, long strength, long offensiveCost, long defensiveCost,
            long anyAuraCost, String element, long limit, String style, ICardEffect cardEffect = null, string name = "")
        {
            Ability = new Ability(damage, magic, tech, strength, offensiveCost, defensiveCost,
            anyAuraCost, element, limit, style, cardEffect, name);
            Ability.CardEffect?.SetCardUser(this);
        }
    }
}
