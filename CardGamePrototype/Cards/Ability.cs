﻿
using CardGamePrototype.Players;

namespace CardGamePrototype.Cards
{
    public struct Ability
    {
        public long Damage { get; }
        public long Magic { get; }
        public long Tech { get; }
        public long Strength { get; }
        public long OffensiveCost { get; }
        public long DefensiveCost { get; }
        public long AnyAuraCost { get; }
        public string Element { get; }
        public long Limit { get; }
        public string Style { get; }
        public ICardEffect CardEffect { get; }
        public string Name { get; }

        public Ability(long damage = 0, long magic = 0, long tech = 0, long strength = 0, long offensiveCost = 0, long defensiveCost = 0,
            long anyAuraCost = 0, string element = "", long limit = 0, string style = "", ICardEffect _CardEffect = null, string name = "")
        {
            Damage = damage;
            Magic = magic;
            Tech = tech;
            Strength = strength;
            OffensiveCost = offensiveCost;
            DefensiveCost = defensiveCost;
            AnyAuraCost = anyAuraCost;
            Element = element;
            Limit = limit;
            Style = style;
            CardEffect = _CardEffect;
            Name = name;
        }
    }
}
