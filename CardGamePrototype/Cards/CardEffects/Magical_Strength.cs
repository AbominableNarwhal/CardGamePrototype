﻿using CardGamePrototype.GameBoardTools;
using CardGamePrototype.MainGame.Actions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CardGamePrototype.GameRunner;

namespace CardGamePrototype.Cards.CardEffects
{
    public class Magical_Strength : ICardEffect
    {
        private CardObject card;
        public IEnumerable<IAction> OnActivate(GameRunnerData gameRunnerData)
        {
            List<IAction> actions = new List<IAction>();
            FighterStatSwapAction fighterStatSwapAction = new FighterStatSwapAction();
            fighterStatSwapAction.Stat1 = FighterStats.Magic;
            fighterStatSwapAction.Stat2 = FighterStats.Strength;
            PlayerCollection col = gameRunnerData.gameBoardController.GetPlayerCollection(card.owner);
            fighterStatSwapAction.Fighter = (FighterCard)col.ActiveFighter.card;
            actions.Add(fighterStatSwapAction);

            return actions;
        }

        public IEnumerable<IAction> OnDeactivate(GameRunnerData gameRunnerData)
        {
            List<IAction> actions = new List<IAction>();
            FighterStatSwapAction fighterStatSwapAction = new FighterStatSwapAction();
            fighterStatSwapAction.Stat1 = FighterStats.Magic;
            fighterStatSwapAction.Stat2 = FighterStats.Strength;
            PlayerCollection col = gameRunnerData.gameBoardController.GetPlayerCollection(card.owner);
            fighterStatSwapAction.Fighter = (FighterCard)col.ActiveFighter.card;
            actions.Add(fighterStatSwapAction);

            return actions;
        }

        public IEnumerable<IAction> OnTurnEnd(GameRunner.GameRunnerData gameRunnerData)
        {
            List<IAction> actions = new List<IAction>();
            EndPlayedCardEffectAction endAction = new EndPlayedCardEffectAction() { TargetCard = card, GameData = gameRunnerData };
            actions.Add(endAction);
            return actions;
        }
        public void SetCardUser(CardObject card)
        {
            this.card = card;
        }
    }
}
