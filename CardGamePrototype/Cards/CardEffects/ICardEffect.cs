﻿using CardGamePrototype.MainGame.Actions;
using System.Collections.Generic;
using static CardGamePrototype.GameRunner;

namespace CardGamePrototype.Cards
{
    public interface ICardEffect
    {
        IEnumerable<IAction> OnActivate(GameRunnerData gameRunnerData);
        IEnumerable<IAction> OnTurnEnd(GameRunnerData gameRunnerData);
        IEnumerable<IAction> OnDeactivate(GameRunnerData gameRunnerData);
        void SetCardUser(CardObject card);
    }
}
