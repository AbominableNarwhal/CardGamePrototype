﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CardGamePrototype.MainGame.Actions;

namespace CardGamePrototype.Cards.CardEffects
{
    public class Guardians_Punishment : ICardEffect
    {
        private AbilityCard card;
        public IEnumerable<IAction> OnActivate(GameRunner.GameRunnerData gameRunnerData)
        {
            List<IAction> actions = new List<IAction>();
            long damage = card.Ability.Damage + card.AttachedFighter.Strength / 100;
            FighterCard targetFighter = (FighterCard)gameRunnerData.gameBoardController.GetPlayerOpponentCollection(card.owner).ActiveFighter.card;
            DamageAction damageAction = new DamageAction() { Damage = damage, TargetFighter = targetFighter };

            actions.Add(damageAction);
            return actions;
        }

        public IEnumerable<IAction> OnDeactivate(GameRunner.GameRunnerData gameRunnerData)
        {
            return null;
        }

        public IEnumerable<IAction> OnTurnEnd(GameRunner.GameRunnerData gameRunnerData)
        {
            return null;
        }

        public void SetCardUser(CardObject card)
        {
            this.card = (AbilityCard)card;
        }
    }
}
