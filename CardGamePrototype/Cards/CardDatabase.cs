﻿using CardGamePrototype.GameBoardTools;
using CardGamePrototype.Players;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine;
using System;
using System.Data.SQLite;

namespace CardGamePrototype.Cards
{
    public class CardDatabase
    {
        public static readonly string FighterType = "Fighter";
        public static readonly string AuraType = "Aura";
        public static readonly string AbilityType = "Ability";
        public static readonly string EquipmentType = "Equipment";
        public static readonly string PowerType = "Power";
        public static readonly string ItemType = "Item";
        private static readonly string DB_NAME = "CardsBase.db";
        private static readonly string CardEffectNameSpace = "CardGamePrototype.Cards.CardEffects.";
        private static CardDatabase cardDatabase;
        private CardDatabase() { }
        public static CardDatabase Instance
        {
            get
            {
                if (cardDatabase == null)
                {
                    cardDatabase = new CardDatabase();
                }
                return cardDatabase;    
            }
        }
        public DeckSet GetPlayerDeck(IPlayer player)
        {
            DeckSet deckSet = new DeckSet(player);
            int fighterCount = 0;
            using (SQLiteConnection con = new SQLiteConnection("Data Source=Content/" + DB_NAME + ";Version=3;"))
            {
                con.Open();
                string cardBackName = GetDeckCardBackName(con, player.DeckName);
                Texture2D cardBackTex = MysterionEngineGame.Game.Content.Load<Texture2D>(cardBackName);
                string sql = GetFullDeckQuery(player.DeckName);
                using (SQLiteCommand command = new SQLiteCommand(sql, con))
                {
                    SQLiteDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        CardObject cardObject = null;
                        long count = (long)reader["Count"];
                        string cardType = (string)reader["Card_Type"];
                        string cardName = (string)reader["Card_Name"];
                        string functionName = (string)reader["Function_Name"];
                        for (long i = 0; i < count; i++)
                        {
                            if (cardType == AuraType)
                            {
                                AuraCard auraCard = new AuraCard(player);
                                cardObject = auraCard;
                            }
                            else if (cardType == FighterType)
                            {
                                FighterCard fighter = new FighterCard((long)reader["HP"],
                                    (long)reader["F_Magic"],
                                    (long)reader["F_Technique"],
                                    (long)reader["F_Strength"],
                                    (long)reader["Equipment_Slots"],
                                    (long)reader["Ability_Slots"],
                                    (string)reader["Class"],
                                    player);
                                cardObject = fighter;
                                if (fighterCount == 0)
                                {
                                    deckSet.Fighters[0] = fighter;
                                }
                                else if (fighterCount == 1)
                                {
                                    deckSet.Fighters[1] = fighter;
                                }
                                else
                                {
                                    deckSet.Fighters[2] = fighter;
                                }
                                fighterCount++;
                                using (SQLiteCommand abilityCom = new SQLiteCommand(GetFighterAbilitiesQuery((string)reader["Card_Name"]), con))
                                {
                                    SQLiteDataReader abilityReader = abilityCom.ExecuteReader();
                                    while (abilityReader.Read())
                                    {
                                        fighter.AddAbility(new Ability((long)abilityReader["Damage"],
                                            0, 0, 0,
                                            (long)abilityReader["Offensive_Cost"],
                                            (long)abilityReader["Defensive_Cost"],
                                            (long)abilityReader["Any_Cost"],
                                            (string)abilityReader["Type"],
                                            (long)abilityReader["Limit_Per_Turn"],
                                            (string)abilityReader["Style"],
                                            FindCardEffectByClassName((string)abilityReader["Function_Name"]),
                                            (string)abilityReader["Ability_Name"]));
                                    }
                                    abilityReader.Close();
                                }
                            }
                            else if (cardType == AbilityType)
                            {
                                AbilityCard abilityCard = new AbilityCard(player);
                                abilityCard.SetAbilityStats((long)reader["Damage"],
                                    (long)reader["A_Magic"],
                                    (long)reader["A_Technique"],
                                    (long)reader["A_Strength"],
                                    (long)reader["A_Offensive_Cost"],
                                    (long)reader["A_Defensive_Cost"],
                                    (long)reader["A_Any_Cost"],
                                    (string)reader["A_Type"],
                                    (long)reader["Limit_Per_Turn"],
                                    (string)reader["Style"],
                                    FindCardEffectByClassName((string)reader["Function_Name"]),
                                    cardName);
                                cardObject = abilityCard;
                            }
                            else if (cardType == EquipmentType)
                            {
                                EquipmentCard equipmentCard = new EquipmentCard((string)reader["E_Type"], player);
                                cardObject = equipmentCard;
                            }
                            else if (cardType == PowerType)
                            {
                                PowerCard powerCard = new PowerCard((long)reader["P_Offensive_Cost"],
                                    (long)reader["P_Defensive_Cost"],
                                    (long)reader["P_Any_Cost"],
                                    player,
                                    FindCardEffectByClassName((string)reader["Function_Name"]));
                                cardObject = powerCard;
                            }
                            else if (cardType == ItemType)
                            {
                                ItemCard itemCard = new ItemCard(player);
                                cardObject = itemCard;
                            }
                            if (cardObject != null)
                            {
                                cardObject.CardName = cardName;
                                cardObject.CardTypeStr = cardType;
                                cardObject.LoadImage(functionName);
                                cardObject.CardBack = cardBackTex;
                                cardObject.FaceUp = true;
                                if (cardObject.CardTypeStr != FighterType)
                                    deckSet.Deck.Add(cardObject);
                            }
                        }
                    }
                    reader.Close();
                }
                con.Close();
            }
            return deckSet;
        }
        private static string GetFullDeckQuery(string deck)
        {
            return "SELECT Deck.Card_ID AS Card_ID, Count, Card_Name, Card_Type, Function_Name, Ability_Slots, Equipment_Slots, HP, Class, F_Magic," +
            "F_Technique, F_Strength, A_Offensive_Cost, A_Defensive_Cost, A_Any_Cost, Damage, A_Magic, A_Technique, A_Strength, A_Type," +
            "Limit_Per_Turn, Style, E_Type, P_Offensive_Cost, P_Defensive_Cost, P_Any_Cost FROM(SELECT Card_ID, Count FROM Deck_Cards " + 
            "WHERE Deck_ID IN(SELECT Deck_ID FROM Deck WHERE Deck_Name = \'" + deck + "\' )) Deck LEFT JOIN(SELECT Card_ID, Card_Name," +
            "Card_Type, Function_Name FROM Main_Cards) Main ON Deck.Card_ID = Main.Card_ID LEFT JOIN(SELECT Ability_Slots, Equipment_Slots, HP, Class," +
            "Magic AS F_Magic, Technique AS F_Technique, Strength AS F_Strength, CARD_ID AS Fighter_ID FROM Fighter) Fighters ON " +
            "Deck.Card_ID = Fighters.Fighter_ID LEFT JOIN(SELECT Offensive_Cost AS A_Offensive_Cost, Defensive_Cost AS A_Defensive_Cost," +
            "Any_Cost AS A_Any_Cost, Damage, Magic AS A_Magic, Technique AS A_Technique, Strength AS A_Strength, Type AS A_Type," +
            "Limit_Per_Turn, Style, CARD_ID AS Ability_ID FROM Ability) Abilities ON  Deck.Card_ID = Abilities.Ability_ID LEFT JOIN" +
            "(SELECT Type AS E_Type, CARD_ID AS Equip_ID FROM Equipment) Equip ON Deck.Card_ID = Equip.Equip_ID LEFT JOIN(SELECT " +
            "Offensive_Cost AS P_Offensive_Cost, Defensive_Cost AS P_Defensive_Cost,Any_Cost AS P_Any_Cost, CARD_ID AS Power_ID FROM " +
            "Power) Power ON Deck.Card_ID = Power.Power_ID; ";
        }
        private static string GetFighterAbilitiesQuery(string fighter)
        {
            return "SELECT * FROM Fighter_Ability WHERE Fighter_ID IN(SELECT Card_ID FROM Main_Cards WHERE Card_Name = \'" + fighter + "\' " +
                "AND Card_Type = \'Fighter\')";
        }
        private static string GetDeckCardBackName(SQLiteConnection con, string deckName)
        {
            string sql = "SELECT Card_Back FROM Deck WHERE Deck_Name = \'" + deckName + "\';";
            string name = "";
            using (SQLiteCommand command = new SQLiteCommand(sql, con))
            {
                SQLiteDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    name = (string)reader["Card_back"];
                }
            }
            return name;
        }
        public static ICardEffect FindCardEffectByClassName(string name)
        {
            if (name != null)
            {
                Type t = Type.GetType(CardEffectNameSpace + name);
                if (t != null)
                {
                    return (ICardEffect)Activator.CreateInstance(t);
                }
            }
            return null;
        }
    }
}
