﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.MainGame.Actions
{
    public interface IAction
    {
        void Execute();
    }
}
