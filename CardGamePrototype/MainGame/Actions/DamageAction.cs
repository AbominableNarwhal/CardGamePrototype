﻿using CardGamePrototype.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.MainGame.Actions
{
    public class DamageAction : IAction
    {
        public FighterCard TargetFighter { get; set; }
        public long Damage { get; set; }

        public void Execute()
        {
            TargetFighter.HP -= Damage;
        }
    }
}
