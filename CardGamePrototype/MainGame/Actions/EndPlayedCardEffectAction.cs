﻿using CardGamePrototype.Cards;
using CardGamePrototype.GameBoardTools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CardGamePrototype.GameRunner;

namespace CardGamePrototype.MainGame.Actions
{
    public class EndPlayedCardEffectAction : IAction
    {
        public CardObject TargetCard { get; set; }
        public GameRunnerData GameData { get; set; }

        public void Execute()
        {
            if (TargetCard != null && GameData != null)
            {
                GameData.DiscardPlayedCard(TargetCard.owner, TargetCard);
                /*PlayerCollection playerCol = GameData.gameBoardController.GetPlayerCollection(TargetCard.owner);
                CascadeContainer playedCardZone = playerCol.PlayedCard;
                if (playedCardZone.ContainsCard(TargetCard))
                {
                    PlayerDeckContainer discardZone = playerCol.Discard;
                    GameData.gameBoardController.MoveCard(TargetCard, playedCardZone, discardZone);
                }*/
            }
        }
    }
}
