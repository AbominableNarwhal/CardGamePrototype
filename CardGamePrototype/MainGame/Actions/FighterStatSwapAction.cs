﻿using CardGamePrototype.Cards;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.MainGame.Actions
{
    public enum FighterStats
    {
        Magic,
        Strength,
        Tech
    }
    public class FighterStatSwapAction : IAction
    {
        public FighterStats Stat1 { get; set;}
        public FighterStats Stat2 { get; set; }
        public FighterCard Fighter { get; set; }

        public FighterStatSwapAction()
        { }
        public FighterStatSwapAction(FighterStats Stat1, FighterStats Stat2, FighterCard card)
        {
            this.Stat1 = Stat1;
            this.Stat2 = Stat2;
            Fighter = card;
        }
        public void Execute()
        {
            if (Fighter == null)
            {
                return;
            }
            FighterStat stat1 = null;
            FighterStat stat2 = null;
            //Find the stats
            switch (Stat1)
            {
                case FighterStats.Magic:
                    stat1 = Fighter.GetMagicRefGet();
                    break;
                case FighterStats.Strength:
                    stat1 = Fighter.GetStrengthRefGet();
                    break;
                case FighterStats.Tech:
                    stat1 = Fighter.GetTechRefGet();
                    break;
            }
            switch (Stat2)
            {
                case FighterStats.Magic:
                    stat2 = Fighter.GetMagicRefGet();
                    break;
                case FighterStats.Strength:
                    stat2 = Fighter.GetStrengthRefGet();
                    break;
                case FighterStats.Tech:
                    stat2 = Fighter.GetTechRefGet();
                    break;
            }
            //Set them
            switch (Stat1)
            {
                case FighterStats.Magic:
                    Fighter.GetMagicRefSet(stat2);
                    break;
                case FighterStats.Strength:
                    Fighter.GetStrengthRefSet(stat2);
                    break;
                case FighterStats.Tech:
                    Fighter.GetTechRefSet(stat2);
                    break;
            }
            switch (Stat2)
            {
                case FighterStats.Magic:
                    Fighter.GetMagicRefSet(stat1);
                    break;
                case FighterStats.Strength:
                    Fighter.GetStrengthRefSet(stat1);
                    break;
                case FighterStats.Tech:
                    Fighter.GetTechRefSet(stat1);
                    break;
            }

        }
    }
}
