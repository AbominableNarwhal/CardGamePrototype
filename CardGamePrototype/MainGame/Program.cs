﻿using System;
using MonoMysterionEngine;

namespace CardGamePrototype
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            StartUpParams param = new StartUpParams();
            param.AppName = "hello guys";
            param.FullScreen = false;
            param.WindowHeight = 1080;
            param.WindowWidth = 1920;
            param.StartActivity = new DebugActivity();
            MysterionEngineGame.Initialize(param);
        }
    }
#endif
}
