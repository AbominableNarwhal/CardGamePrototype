﻿using CardGamePrototype.Cards;
using CardGamePrototype.GameBoardTools;
using CardGamePrototype.MainGame;
using CardGamePrototype.MainGame.Actions;
using CardGamePrototype.Players;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MonoMysterionEngine;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using static CardGamePrototype.GameBoardTools.GameBoardInput;

namespace CardGamePrototype
{
    public class GameRunner
    {
        private readonly GameRunnerData memberData;
        private GameStateObjects gameStates;
        private bool spaceKeyDown = false;
        private GameRunnerUI gameUi;
        private CardObject updateClickCard;
        public GameRunner()
        {
            memberData = new GameRunnerData();
            memberData.player1 = new HumanPlayer();
            memberData.player2 = new HumanPlayer();
            memberData.player1DeckSet = CardDatabase.Instance.GetPlayerDeck(memberData.player1);
            memberData.player2DeckSet = CardDatabase.Instance.GetPlayerDeck(memberData.player2);
            GameBoard gameBoard = GameBoardLoader.LoadGameBoard(
                "Content/boardlayout1.tmx",
                memberData.player1,
                memberData.player1DeckSet,
                memberData.player2,
                memberData.player2DeckSet);
            memberData.gameBoardController = new GameBoardController(gameBoard);
            memberData.gameBoardController.gameBoardInput.GameboardInputEvent += OnGameBoardInputEvent;

            gameUi = GameRunnerUILoader.LoadUI("Content/gamerunnerui.tmx");
            gameUi.SetUIHandler(OnUIInput);
        }
        public void GameStart()
        {
            //Add all cards to the engine
            AddDeckToEngine(memberData.player1DeckSet);
            AddDeckToEngine(memberData.player2DeckSet);
            MysterionEngineGame.Game.ObjectLocator.AddObject(memberData.gameBoardController);
            memberData.gameBoardController.StartGame();
            gameStates = new GameStateObjects(memberData);
            gameStates.CurrentState = gameStates.setupState;

            MysterionEngineGame.Game.ObjectLocator.AddObject(gameUi);
        }
        public void GameUpdate()
        {
            if (!memberData.gameBoardController.gameBoardRenderer.IsAnimating())
            {
                if (!memberData.ProcessCardEffects())
                {
                    gameStates.CurrentState.OnUpdate();
                }
            }
            if (updateClickCard != null)
            {
                gameUi.DisplayCard(updateClickCard);
                updateClickCard = null;
            }
        }
        public void GameEnd()
        {
            RemoveDeckFromEngine(memberData.player1DeckSet);
            RemoveDeckFromEngine(memberData.player2DeckSet);
            memberData.gameBoardController.gameBoardInput.GameboardInputEvent -= OnGameBoardInputEvent;
            memberData.gameBoardController.EndGame();
            MysterionEngineGame.Game.ObjectLocator.RemoveObject(gameUi);
        }
        public void OnGameBoardInputEvent(object sender, GameBoardMouseEventArgs e)
        {
            gameStates.CurrentState.OnGameBoardInputEvent(e);
            if (e.EventType == CardInputEventType.MouseEnter)
            {
                Console.WriteLine("Mouse enter " + e.Subject.CardName + "!");
            }
            else if (e.EventType == CardInputEventType.MouseExit)
            {
                Console.WriteLine("Mouse exit " + e.Subject.CardName + "!");
            }
            else if (e.EventType == CardInputEventType.MouseClicked)
            {
                Console.WriteLine("Mouse clicked " + e.Subject.CardName + "!");
                updateClickCard = e.Subject;
            }
        }
        public void InputUpdate(KeyboardState keyboardState, MouseState mouseState)
        {
            if (keyboardState.IsKeyDown(Keys.Space))
            {
                spaceKeyDown = true;
            }
            else if (keyboardState.IsKeyUp(Keys.Space))
            {
                if (spaceKeyDown)
                {
                    gameStates.CurrentState.OnEndTurnSignal();
                }
                spaceKeyDown = false;
            }
        }
        public void OnUIInput(FighterCard fighter, Ability ability)
        {
            gameStates.CurrentState.ActivateAbilitySignal(fighter, ability);
        }
        private void AddDeckToEngine(DeckSet deckSet)
        {
            foreach (CardObject card in deckSet.Deck)
            {
                MysterionEngineGame.Game.ObjectLocator.AddObject(card);
            }
            //Fighters
            foreach (CardObject card in deckSet.Fighters)
            {
                MysterionEngineGame.Game.ObjectLocator.AddObject(card);
            }
        }
        private void RemoveDeckFromEngine(DeckSet deckSet)
        {
            foreach (CardObject card in deckSet.Deck)
            {
                MysterionEngineGame.Game.ObjectLocator.RemoveObject(card);
            }
            //Fighters
            foreach (CardObject card in deckSet.Fighters)
            {
                MysterionEngineGame.Game.ObjectLocator.RemoveObject(card);
            }
        }
        public class GameRunnerData
        {
            public GameBoardController gameBoardController;
            public IPlayer player1;
            public IPlayer player2;
            public IPlayer offensivePlayer;
            public IPlayer defensivePlayer;
            public DeckSet player1DeckSet;
            public DeckSet player2DeckSet;
            public CardObject playedCard;
            public Ability offensiveAbility;
            public Ability defensiveAbility;
            public List<IAction> actionQueue;
            public List<ICardEffect> cardEffects;
            public GameRunnerData()
            {
                actionQueue = new List<IAction>();
                cardEffects = new List<ICardEffect>();
            }
            public void TriggerOnTurnEndEvent()
            {
                foreach (ICardEffect cardEffect in cardEffects)
                {
                    AddActions(cardEffect.OnTurnEnd(this));
                }
            }
            public void DrawCard(IPlayer player)
            {
                PlayerCollection playerCollection = gameBoardController.GetPlayerCollection(player);
                gameBoardController.MoveCard(
                    playerCollection.Deck.GetTopCard(), //Card
                    playerCollection.Deck, //Source
                    playerCollection.Hand); //Destination
            }
            public void PlayPowerItemCardFromHand(IPlayer player, CardObject card)
            {
                PlayerCollection playerCollection = gameBoardController.GetPlayerCollection(player);
                if (playerCollection.Hand.ContainsCard(card))
                {
                    gameBoardController.MoveCard(
                        card, //Card
                        playerCollection.Hand, //Source
                        playerCollection.PlayedCard); //Destination
                }
                cardEffects.Add(GetPowerItemCardEffect(card));
            }
            public void PlayAuraFromHand(IPlayer player, CardObject card)
            {
                PlayerCollection playerCollection = gameBoardController.GetPlayerCollection(player);
                if (playerCollection.Hand.ContainsCard(card))
                {
                    gameBoardController.MoveCard(
                        card, //Card
                        playerCollection.Hand, //Source
                        playerCollection.AuraZone); //Destination
                }
                playerCollection.AuraPoolCounter.UpdateText();
            }
            public ICardEffect GetPowerItemCardEffect(CardObject card)
            {
                ICardEffect cardEffect = null;
                if (card != null)
                {
                    if (card.Type == CardObject.CardType.Item)
                    {
                        //cardEffect = ((ItemCard)card).Card
                    }
                    else if (card.Type == CardObject.CardType.Power)
                    {
                        cardEffect = ((PowerCard)card).PowerCardEffect;
                    }
                }
                return cardEffect;
            }
            /*card must be either an item or power*/
            public void DiscardPlayedCard(IPlayer player, CardObject card)
            {
                PlayerCollection playerCollection = gameBoardController.GetPlayerCollection(player);
                if (playerCollection.PlayedCard.ContainsCard(card))
                {
                    gameBoardController.MoveCard(
                        card, //Card
                        playerCollection.PlayedCard, //Source
                        playerCollection.Discard); //Destination
                }
                ICardEffect cardEffect = GetPowerItemCardEffect(card);
                if (cardEffect != null)
                {
                    AddActions(cardEffect.OnDeactivate(this));
                }
                cardEffects.Remove(cardEffect);
            }
            public void AttachAbilityCardFromHand(IPlayer player, FighterCard fighterCard, AbilityCard abilityCard)
            {
                PlayerCollection playerCollection = gameBoardController.GetPlayerCollection(player);
                if (playerCollection.ActiveFighter.card == fighterCard)
                {
                    fighterCard.AttachAbilityCard(abilityCard);
                    gameBoardController.MoveCard(
                        abilityCard, //Card
                        playerCollection.Hand, //Source
                        playerCollection.AbilitySlots); //Destination
                    cardEffects.Add(abilityCard.Ability.CardEffect);
                }
                else if (playerCollection.SupportFighters[0].card == fighterCard ||
                    playerCollection.SupportFighters[1].card == fighterCard)
                {
                    fighterCard.AttachAbilityCard(abilityCard);
                    playerCollection.Hand.RemoveCard(abilityCard);
                    abilityCard.IsVisable = false;
                }
            }
            public void AttachEquipmentCardFromHand(IPlayer player, FighterCard fighterCard, EquipmentCard equipmentCard)
            {
                PlayerCollection playerCollection = gameBoardController.GetPlayerCollection(player);
                if (playerCollection.ActiveFighter.card == fighterCard)
                {
                    fighterCard.AttachEquipmentCard(equipmentCard);
                    gameBoardController.MoveCard(
                        equipmentCard, //Card
                        playerCollection.Hand, //Source
                        playerCollection.EquipmentSlots); //Destination
                }
                else if (playerCollection.SupportFighters[0].card == fighterCard ||
                    playerCollection.SupportFighters[1].card == fighterCard)
                {
                    fighterCard.AttachEquipmentCard(equipmentCard);
                    playerCollection.Hand.RemoveCard(equipmentCard);
                    equipmentCard.IsVisable = false;
                }
            }
            /// <summary>
            /// Processes the next Card Effect object in the action queue.
            /// </summary>
            /// <returns>True if the was a card effect to processes and false
            /// if the action queue was empty.</returns>
            public bool ProcessCardEffects()
            {
                if (actionQueue.Count > 0)
                {
                    IAction action = actionQueue.Last();
                    action.Execute();
                    actionQueue.Remove(action);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            public void AddActions(IEnumerable<IAction> newActions)
            {
                if (newActions != null)
                {
                    actionQueue.AddRange(newActions.Reverse());
                }
            }
        }
        /*A container to allow to reuse of states*/
        private class GameStateObjects
        {
            public Stack<GameState> pushDownStack;
            public GameSetupState setupState;
            public GameDrawState drawState;
            public GameCombatState combatState;
            public GamePowerItemState powerState;
            public GameDefensiveResponceState defensiveResponceState;
            public GameOffensiveResponceState offensiveResponceState;
            public GameAbilityResolveState abilityResolveState;
            private GameState currentState;
            public GameState CurrentState
            {
                get
                {
                    return currentState;
                }
                set
                {
                    currentState?.OnExit();
                    currentState = value;
                    currentState.OnEnter();
                }
            }

            public GameStateObjects(GameRunnerData gameRunnerData)
            {
                setupState = new GameSetupState(gameRunnerData, this);
                drawState = new GameDrawState(gameRunnerData, this);
                combatState = new GameCombatState(gameRunnerData, this);
                powerState = new GamePowerItemState(gameRunnerData, this);
                defensiveResponceState = new GameDefensiveResponceState(gameRunnerData, this);
                offensiveResponceState = new GameOffensiveResponceState(gameRunnerData, this);
                abilityResolveState = new GameAbilityResolveState(gameRunnerData, this);
                pushDownStack = new Stack<GameState>();
            }
        }
        private abstract class GameState
        {
            protected GameRunnerData gameRunnerData;
            protected GameStateObjects gameStates;
            public GameState(GameRunnerData gameRunnerData, GameStateObjects gameStates)
            {
                this.gameRunnerData = gameRunnerData;
                this.gameStates = gameStates;
                gameRunnerData.offensivePlayer = gameRunnerData.player1;
                gameRunnerData.defensivePlayer = gameRunnerData.player2;
            }

            public abstract void OnEnter();
            public abstract void OnUpdate();
            public abstract void OnExit();
            public virtual void OnGameBoardInputEvent(GameBoardMouseEventArgs e) { }
            public virtual void OnEndTurnSignal() { }
            public virtual void ActivateAbilitySignal(FighterCard fighter, Ability ability) { }
        }
        
        private class GameSetupState : GameState
        {
            public GameSetupState(GameRunnerData gameRunnerData, GameStateObjects gameStates) : base(gameRunnerData, gameStates)
            { }
            public override void OnUpdate()
            {
                for(int i = 0; i < 6; i++)
                {
                    gameRunnerData.DrawCard(gameRunnerData.player1);
                }
                for (int i = 0; i < 6; i++)
                {
                    gameRunnerData.DrawCard(gameRunnerData.player2);
                }
                gameStates.CurrentState = gameStates.drawState;
            }
            public override void OnEnter()
            { }
            public override void OnExit()
            { }
        }
        private class GameDrawState : GameState
        {
            public GameDrawState(GameRunnerData gameRunnerData, GameStateObjects gameStates) : base(gameRunnerData, gameStates)
            { }
            public override void OnEnter()
            {

            }

            public override void OnExit()
            { }

            public override void OnUpdate()
            {
                gameRunnerData.DrawCard(gameRunnerData.offensivePlayer);
                gameStates.CurrentState = gameStates.combatState;
            }
        }
        private class GameCombatState : GameState
        {
            public GameCombatState(GameRunnerData gameRunnerData, GameStateObjects gameStates) : base(gameRunnerData, gameStates)
            { }
            private void IssueQuery()
            {
                CombatQuery query = new CombatQuery();
                gameRunnerData.offensivePlayer.OnQuerySubmitted(query, gameRunnerData);
            }
            public override void OnEnter()
            {
                IssueQuery();
            }

            public override void OnExit()
            { }

            public override void OnUpdate()
            {
                IQueryAnswer answer = gameRunnerData.offensivePlayer.GetQueryAnswer();
                if (answer != null)
                {
                    CombatQueryAnswer combatQueryAnswer = (CombatQueryAnswer)answer;
                    if (answer.Type == QueryAnswerType.PlayPowerCard || answer.Type == QueryAnswerType.PlayItemCard)
                    {
                        gameRunnerData.playedCard = combatQueryAnswer.Card;
                        gameStates.pushDownStack.Push(this);
                        gameStates.CurrentState = gameStates.powerState;
                    }
                    else if (answer.Type == QueryAnswerType.AttachAbilityCard)
                    {
                        gameRunnerData.AttachAbilityCardFromHand(gameRunnerData.offensivePlayer, combatQueryAnswer.TargetFighter,
                            (AbilityCard)combatQueryAnswer.Card);
                        IssueQuery();
                    }
                    else if (answer.Type == QueryAnswerType.ActivateAbility)
                    {
                        gameRunnerData.offensiveAbility = combatQueryAnswer.AbilityToActivate;
                        gameStates.CurrentState = gameStates.defensiveResponceState;
                    }
                    else if (answer.Type == QueryAnswerType.PlayAura)
                    {
                        gameRunnerData.PlayAuraFromHand(gameRunnerData.offensivePlayer, combatQueryAnswer.Card);
                        IssueQuery();
                    }
                    else if (answer.Type == QueryAnswerType.AttachEquipmentCard)
                    {
                        gameRunnerData.AttachEquipmentCardFromHand(gameRunnerData.offensivePlayer, combatQueryAnswer.TargetFighter,
                            (EquipmentCard)combatQueryAnswer.Card);
                        IssueQuery();
                    }
                    else if (answer.Type == QueryAnswerType.None)
                    {
                        gameRunnerData.TriggerOnTurnEndEvent();
                        IPlayer temp = gameRunnerData.offensivePlayer;
                        gameRunnerData.offensivePlayer = gameRunnerData.defensivePlayer;
                        gameRunnerData.defensivePlayer = temp;
                        gameStates.CurrentState = gameStates.drawState;
                    }
                }
            }
            public override void OnGameBoardInputEvent(GameBoardMouseEventArgs e)
            {
                if (e.EventType == CardInputEventType.MouseClicked && e.Subject.owner == gameRunnerData.offensivePlayer)
                {
                    if (e.Location == ContainerType.Hand)
                    {
                        gameRunnerData.offensivePlayer.OnCardInHandClicked(e.Subject);
                    }
                    else if (e.Location == ContainerType.ActiveFighter)
                    {
                        gameRunnerData.offensivePlayer.OnActiveFighterClicked((FighterCard)e.Subject);
                    }
                    else if (e.Location == ContainerType.SupportFighter)
                    {
                        gameRunnerData.offensivePlayer.OnSupportFighterClicked((FighterCard)e.Subject);
                    }
                    else if (e.Location == ContainerType.AbilitySlots)
                    {
                        gameRunnerData.offensivePlayer.OnAttachedAblilityCardClicked((AbilityCard)e.Subject);
                    }
                }
            }
            public override void OnEndTurnSignal()
            {
                base.OnEndTurnSignal();
                gameRunnerData.offensivePlayer.OnEndTurnSignal();
            }
            public override void ActivateAbilitySignal(FighterCard fighter, Ability ability)
            {
                base.ActivateAbilitySignal(fighter, ability);
                IPlayer player = gameRunnerData.offensivePlayer;
                if (fighter.owner == player)
                {
                    player.ActivateAbilitySignal(ability);
                }
            }
        }
        private class GameDefensiveResponceState : GameState
        {
            public GameDefensiveResponceState(GameRunnerData gameRunnerData, GameStateObjects gameStates) : base(gameRunnerData, gameStates)
            { }

            private void IssueQuery()
            {
                //TODO make defensive responce query
                CombatQuery query = new CombatQuery();
                gameRunnerData.defensivePlayer.OnQuerySubmitted(query, gameRunnerData);
            }
            public override void OnEnter()
            {
                IssueQuery();
            }

            public override void OnExit()
            { }

            public override void OnUpdate()
            {
                IQueryAnswer answer = gameRunnerData.defensivePlayer.GetQueryAnswer();
                if (answer != null)
                {
                    CombatQueryAnswer combatQueryAnswer = (CombatQueryAnswer)answer;
                    if (answer.Type == QueryAnswerType.PlayPowerCard)
                    {
                        gameRunnerData.playedCard = combatQueryAnswer.Card;
                        gameStates.pushDownStack.Push(this);
                        gameStates.CurrentState = gameStates.powerState;
                    }
                    else if (answer.Type == QueryAnswerType.ActivateAbility)
                    {
                        gameRunnerData.defensiveAbility = combatQueryAnswer.AbilityToActivate;
                        gameStates.CurrentState = gameStates.offensiveResponceState;
                    }
                    else if (answer.Type == QueryAnswerType.None)
                    {
                        gameStates.CurrentState = gameStates.abilityResolveState;
                    }
                }
            }

            public override void OnGameBoardInputEvent(GameBoardMouseEventArgs e)
            {
                if (e.EventType == CardInputEventType.MouseClicked && e.Subject.owner == gameRunnerData.defensivePlayer)
                {
                    if (e.Location == ContainerType.Hand)
                    {
                        gameRunnerData.defensivePlayer.OnCardInHandClicked(e.Subject);
                    }
                    else if (e.Location == ContainerType.ActiveFighter)
                    {
                        gameRunnerData.defensivePlayer.OnActiveFighterClicked((FighterCard)e.Subject);
                    }
                    else if (e.Location == ContainerType.AbilitySlots)
                    {
                        gameRunnerData.defensivePlayer.OnAttachedAblilityCardClicked((AbilityCard)e.Subject);
                    }
                }
            }
            public override void OnEndTurnSignal()
            {
                base.OnEndTurnSignal();
                gameRunnerData.defensivePlayer.OnEndTurnSignal();
            }
        }
        private class GameOffensiveResponceState : GameState
        {
            public GameOffensiveResponceState(GameRunnerData gameRunnerData, GameStateObjects gameStates) : base(gameRunnerData, gameStates)
            { }

            private void IssueQuery()
            {
                //TODO make defensive responce query
                CombatQuery query = new CombatQuery();
                gameRunnerData.offensivePlayer.OnQuerySubmitted(query, gameRunnerData);
            }
            public override void OnEnter()
            {
                IssueQuery();
            }

            public override void OnExit()
            { }

            public override void OnUpdate()
            {
                IQueryAnswer answer = gameRunnerData.offensivePlayer.GetQueryAnswer();
                if (answer != null)
                {
                    CombatQueryAnswer combatQueryAnswer = (CombatQueryAnswer)answer;
                    if (answer.Type == QueryAnswerType.PlayPowerCard)
                    {
                        gameRunnerData.playedCard = combatQueryAnswer.Card;
                        gameStates.pushDownStack.Push(this);
                        gameStates.CurrentState = gameStates.powerState;
                    }
                    else if (answer.Type == QueryAnswerType.None)
                    {
                        gameStates.CurrentState = gameStates.abilityResolveState;
                    }
                }
            }

            public override void OnGameBoardInputEvent(GameBoardMouseEventArgs e)
            {
                if (e.EventType == CardInputEventType.MouseClicked && e.Subject.owner == gameRunnerData.offensivePlayer)
                {
                    if (e.Location == ContainerType.Hand)
                    {
                        gameRunnerData.offensivePlayer.OnCardInHandClicked(e.Subject);
                    }
                    else if (e.Location == ContainerType.ActiveFighter)
                    {
                        gameRunnerData.offensivePlayer.OnActiveFighterClicked((FighterCard)e.Subject);
                    }
                    else if (e.Location == ContainerType.AbilitySlots)
                    {
                        gameRunnerData.offensivePlayer.OnAttachedAblilityCardClicked((AbilityCard)e.Subject);
                    }
                }
            }
            public override void OnEndTurnSignal()
            {
                base.OnEndTurnSignal();
                gameRunnerData.offensivePlayer.OnEndTurnSignal();
            }
        }
        private class GameAbilityResolveState : GameState
        {
            public GameAbilityResolveState(GameRunnerData gameRunnerData, GameStateObjects gameStates) : base(gameRunnerData, gameStates)
            { }
            public override void OnEnter()
            {
                Ability offensiveAbility = gameRunnerData.offensiveAbility;
                gameRunnerData.AddActions(offensiveAbility.CardEffect?.OnActivate(gameRunnerData));
                Ability defensiveAbility = gameRunnerData.defensiveAbility;
                if (defensiveAbility.CardEffect != null)
                {
                    gameRunnerData.AddActions(defensiveAbility.CardEffect?.OnActivate(gameRunnerData));
                }
            }

            public override void OnExit()
            {
                gameRunnerData.offensiveAbility = gameRunnerData.defensiveAbility = new Ability();
            }

            public override void OnUpdate()
            {
                gameStates.CurrentState = gameStates.combatState;
            }
        }
        private class GamePowerItemState : GameState
        {
            public GamePowerItemState(GameRunnerData gameRunnerData, GameStateObjects gameStates) : base(gameRunnerData, gameStates)
            { }
            public override void OnEnter()
            {
                gameRunnerData.PlayPowerItemCardFromHand(gameRunnerData.playedCard.owner, gameRunnerData.playedCard);
                if (gameRunnerData.playedCard.Type == CardObject.CardType.Power)
                {
                    PowerCard playedPowerCard = (PowerCard)gameRunnerData.playedCard;
                    ICardEffect cardEffect = playedPowerCard.PowerCardEffect;
                    gameRunnerData.AddActions(cardEffect?.OnActivate(gameRunnerData));
                }
            }
            public override void OnUpdate()
            {
                gameStates.CurrentState = gameStates.pushDownStack.Pop();
            }
            public override void OnExit()
            { }
        }
    }
}
