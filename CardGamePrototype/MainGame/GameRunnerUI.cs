﻿using CardGamePrototype.Cards;
using Microsoft.Xna.Framework;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.MainGame
{
    public class GameRunnerUI : GameObject
    {
        private List<GameObject> itemList;
        public delegate void AbilityUIHandler(FighterCard fighter, Ability ability);
        private AbilityUIHandler abilityUIHandler;
        private CardObject displayedCard;
        public GameRunnerUI()
        {
            itemList = new List<GameObject>();
        }
        public void SetUIHandler(AbilityUIHandler abilityUIHandler)
        {
            this.abilityUIHandler = abilityUIHandler;
        }
        public void DisplayCard(CardObject card)
        {
            ClearItems();
            displayedCard = card;
            CardArt.SpriteImage.Texture = card.Texture;
            if (card.Type != CardObject.CardType.Fighter)
            {
                AbilityTitle.RenderComponent.Visable = false;
                return;
            }
            AbilityTitle.RenderComponent.Visable = true;
            FighterCard fighter = (FighterCard)card;

            int i = 0;
            foreach (Ability ability in fighter.Abilities)
            {
                MenuItemPrefab.AbilityName.FontObj.Text = ability.Name;

                GameObject obj = MenuItemPrefab.CreateMenuItem(OnUIInput, i);
                Vector2 pos = ListItemPosition;
                pos.Y += MenuItemPrefab.ItemImage.SpriteImage.Texture.Height * i;
                obj.Transforms.Position = new Vector3(pos, 0);
                itemList.Add(obj);
                AddChild(obj);
                i++;
            }

        }
        private void OnUIInput(GameObject gameObject, Button.Events buttonEvent)
        {
            if (buttonEvent == Button.Events.Click)
            {
                int id = (int)gameObject.Id;
                if (displayedCard.Type == CardObject.CardType.Fighter)
                {
                    FighterCard fighter = (FighterCard)displayedCard;
                    abilityUIHandler?.Invoke(fighter, fighter.Abilities[id]);
                }
            }
        }
        private void ClearItems()
        {
            foreach (GameObject item in itemList)
            {
                RemoveChild(item);
            }
            itemList.Clear();
        }
        public Vector2 ListItemPosition;
        public SpriteObject CardArt { get; set; }
        public TextObject AbilityTitle { get; set; }
        public GameRunnerUIMenuItemPrefab MenuItemPrefab { get; set; }
    }
    public class GameRunnerUIMenuItemPrefab : IObjectGroup
    {
        public string Name { get; set; }
        public long Id { get; set; }
        public TextObject AbilityName { get; set; }
        public SpriteObject ItemImage { get; set; }
        public Button ActivateButton { get; set; }
        public GameObject CreateMenuItem(Button.ButtonEventHandler buttonHandler, long buttonId)
        {
            GameObject menuItem = new GameObject();
            TextObject abilityName = new TextObject(AbilityName);
            SpriteObject itemImage = new SpriteObject(ItemImage);
            Button activateButton = new Button(ActivateButton);
            activateButton.ButtonObject.Id = buttonId;
            activateButton.ClickHandler = buttonHandler;
            menuItem.AddChild(itemImage);
            menuItem.AddChild(abilityName);
            menuItem.AddChild(activateButton.ButtonObject);

            return menuItem;
        }
    }
}
