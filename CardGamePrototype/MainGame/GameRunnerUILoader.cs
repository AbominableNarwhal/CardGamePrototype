﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.MainGame
{
    public class GameRunnerUILoader
    {
        private static readonly string MenuName = "Menu";
        private static readonly string ListItemName = "List Item";
        private class UILoader : DefaultMapLoader
        {
            public override IObjectGroup NewObjectGroup(string name, long id, float x, float y)
            {
                if (name == MenuName)
                {
                    GameRunnerUI ui = new GameRunnerUI
                    {
                        Name = name,
                        Id = id
                    };
                    ui.Transforms.Position = new Vector3(x, y, 0);
                    return ui;
                }
                else if (name == ListItemName)
                {
                    GameRunnerUIMenuItemPrefab menuItemPrefab = new GameRunnerUIMenuItemPrefab
                    {
                        Name = name,
                        Id = id
                    };
                    return menuItemPrefab;
                }
                return base.NewObjectGroup(name, id, x, y);
            }
            public override void NewObjectData(IObjectGroup group, string name, float x, float y, float width, float height, float rotation,
                Texture2D texture, TextData? textData, Dictionary<string, string> properties)
            {
                if (group.Name != MenuName && group.Name != ListItemName)
                {
                    base.NewObjectData(group, name, x, y, width, height, rotation, texture, textData, properties);
                    return;
                }

                if (group.Name == MenuName)
                {
                    GameRunnerUI gameRunnerUI = (GameRunnerUI)group;
                    if (name == "card-art")
                    {
                        SpriteObject spriteObject = new SpriteObject();
                        LoadGameObjectData(spriteObject, name, x, y, rotation, new Vector2(1, 1));
                        gameRunnerUI.CardArt = spriteObject;
                        gameRunnerUI.AddChild(spriteObject);
                    }
                    else if (name == "abilities-title")
                    {
                        TextObject abilityTitle = new TextObject()
                        { FontObj = textData.Value.font };
                        abilityTitle.RenderComponent.Visable = false;
                        LoadGameObjectData(abilityTitle, name, x, y, rotation, new Vector2(1, 1));
                        gameRunnerUI.AbilityTitle = abilityTitle;
                        gameRunnerUI.AddChild(abilityTitle);
                    }
                    else if (name == "list-start")
                    {
                        gameRunnerUI.ListItemPosition = new Vector2(x, y);
                    }
                    else
                    {
                        base.NewObjectData(group, name, x, y, width, height, rotation, texture, textData, properties);
                    }
                }
                else if (group.Name == ListItemName)
                {
                    GameRunnerUIMenuItemPrefab menuItemPrefab = (GameRunnerUIMenuItemPrefab)group;
                    if (name == "ability-name")
                    {
                        TextObject abilityName = new TextObject();
                        LoadTextRenderData(abilityName, out Vector2 scale, textData, out TextRenderComponent renderComponent);
                        abilityName.RenderComponent = renderComponent;
                        LoadGameObjectData(abilityName, name, x, y, rotation, scale);
                        menuItemPrefab.AbilityName = abilityName;
                    }
                    else if (name == "menu-item")
                    {
                        SpriteObject itemImage = new SpriteObject();
                        itemImage.SpriteImage.Texture = texture;
                        LoadGameObjectData(itemImage, name, x, y, rotation, new Vector2(1, 1));
                        menuItemPrefab.ItemImage = itemImage;
                    }
                    else if (name == "item-button")
                    {
                        Texture2D pressedTexture = ResourceLoader.Load<Texture2D>(properties["pressed-image"]);
                        Button button = new Button(texture, pressedTexture);
                        LoadGameObjectData(button.ButtonObject, name, x, y, rotation, new Vector2(1, 1));
                        menuItemPrefab.ActivateButton = button;
                    }
                }

            }
        }

        public static GameRunnerUI LoadUI(string fileName)
        {
            UILoader loader = new UILoader();
            GameWorldMap map = WorldMapLoader.LoadMap(fileName, loader);
            GameRunnerUI ui = (GameRunnerUI)(from val in map.Objects where val.Name == MenuName select val).FirstOrDefault();
            GameRunnerUIMenuItemPrefab menuItemPrefab = (GameRunnerUIMenuItemPrefab)(from val in map.Objects where val.Name == ListItemName select val).FirstOrDefault();
            ui.MenuItemPrefab = menuItemPrefab;
            return ui;
        }
    }
}
