﻿using CardGamePrototype.GameBoardTools;
using CardGamePrototype.Players;
using Microsoft.Xna.Framework;
using System;
using System.Linq;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine.Utilities;
using System.Collections.Generic;

namespace CardGamePrototype.MainGame
{
    public class GameBoardLoader
    {
        private static readonly string GameBoard_Name = "game board";
        private class BoardLoader : DefaultMapLoader
        {
            public IPlayer player1;
            public DeckSet player1DeckSet;
            public IPlayer player2;
            public DeckSet player2DeckSet;
            private float cardWidth;
            private float cardHeight;
            public override IObjectGroup NewObjectGroup(string name, long id, float x, float y)
            {
                if (name != GameBoard_Name)
                {
                    return base.NewObjectGroup(name, id, x, y);
                }
                GameBoard gameBoard = new GameBoard(player1, player2);
                gameBoard.Name = name;
                gameBoard.Id = id;
                cardWidth = player1DeckSet.Fighters[0].Width;
                cardHeight = player1DeckSet.Fighters[0].Height;
                return gameBoard;
            }
            public override void NewObjectData(IObjectGroup group, string name, float x, float y, float width, float height, float rotation,
                Texture2D texture, TextData? textData, Dictionary<string, string> properties)
            {
                if (group.Name != GameBoard_Name)
                {
                    base.NewObjectData(group, name, x, y, width, height, rotation, texture, textData, properties);
                    return;
                }

                GameBoard gameBoard = (GameBoard)group;

                float r = MathHelper.ToRadians(rotation);
                float sx = width / cardWidth;
                float sy = height / cardHeight;
                float s = Math.Min(sx, sy);
                CardLayout cardLayout = new CardLayout() { Position = new Vector2(x, y), Scale = s, Rotation = r, FaceUp = true, IsVisable = true };

                if (name == "Player1Hand")
                {
                    gameBoard.player1.Hand = new PlayerHandContainer(new Vector2(x, y), (int)width, (int)height, s, cardWidth);
                    gameBoard.player1.Hand.Type = ContainerType.Hand;
                }
                else if (name == "Player2Hand")
                {
                    gameBoard.player2.Hand = new PlayerHandContainer(new Vector2(x, y), (int)width, (int)height, s, cardWidth);
                    gameBoard.player2.Hand.Type = ContainerType.Hand;
                }
                else if (name == "Player1ActiveFighter")
                {
                    gameBoard.player1.ActiveFighter = new SingleCardContainer(cardLayout, player1DeckSet.Fighters[0]);
                    gameBoard.player1.ActiveFighter.Type = ContainerType.ActiveFighter;
                }
                else if (name == "Player2ActiveFighter")
                {
                    gameBoard.player2.ActiveFighter = new SingleCardContainer(cardLayout, player2DeckSet.Fighters[0]);
                    gameBoard.player2.ActiveFighter.Type = ContainerType.ActiveFighter;
                }
                else if (name == "Player1Deck")
                {
                    cardLayout.FaceUp = false;
                    gameBoard.player1.Deck = new PlayerDeckContainer(cardLayout);
                    gameBoard.player1.Deck.AddRange(player1DeckSet.Deck);
                    gameBoard.player1.Deck.Type = ContainerType.Deck;
                }
                else if (name == "Player2Deck")
                {
                    cardLayout.FaceUp = false;
                    gameBoard.player2.Deck = new PlayerDeckContainer(cardLayout);
                    gameBoard.player2.Deck.AddRange(player2DeckSet.Deck);
                    gameBoard.player2.Deck.Type = ContainerType.Deck;
                }
                else if (name == "Player1Discard")
                {
                    gameBoard.player1.Discard = new PlayerDeckContainer(cardLayout);
                    gameBoard.player1.Discard.Type = ContainerType.Discard;
                }
                else if (name == "Player2Discard")
                {
                    gameBoard.player2.Discard = new PlayerDeckContainer(cardLayout);
                    gameBoard.player2.Discard.Type = ContainerType.Discard;
                }
                else if (name == "Player1SupportFighter1")
                {
                    gameBoard.player1.SupportFighters[0] = new SingleCardContainer(cardLayout, player1DeckSet.Fighters[1]);
                    gameBoard.player1.SupportFighters[0].Type = ContainerType.SupportFighter;
                }
                else if (name == "Player1SupportFighter2")
                {
                    gameBoard.player1.SupportFighters[1] = new SingleCardContainer(cardLayout, player1DeckSet.Fighters[2]);
                    gameBoard.player1.SupportFighters[1].Type = ContainerType.SupportFighter;
                }
                else if (name == "Player2SupportFighter1")
                {
                    gameBoard.player2.SupportFighters[0] = new SingleCardContainer(cardLayout, player2DeckSet.Fighters[1]);
                    gameBoard.player2.SupportFighters[0].Type = ContainerType.SupportFighter;
                }
                else if (name == "Player2SupportFighter2")
                {
                    gameBoard.player2.SupportFighters[1] = new SingleCardContainer(cardLayout, player2DeckSet.Fighters[2]);
                    gameBoard.player2.SupportFighters[1].Type = ContainerType.SupportFighter;
                }
                else if (name == "Player1PlayedCard")
                {
                    gameBoard.player1.PlayedCard = new CascadeContainer((int)x, (int)y, r, s, cardWidth, true, 100);
                    gameBoard.player1.PlayedCard.Type = ContainerType.Played;
                }
                else if (name == "Player2PlayedCard")
                {
                    gameBoard.player2.PlayedCard = new CascadeContainer((int)x, (int)y, r, s, cardWidth);
                    gameBoard.player2.PlayedCard.Type = ContainerType.Played;
                }
                else if (name == "Player1Abilities")
                {
                    gameBoard.player1.AbilitySlots = new CascadeContainer((int)x, (int)y, r, s, cardWidth);
                    gameBoard.player1.AbilitySlots.Type = ContainerType.AbilitySlots;
                }
                else if (name == "Player2Abilities")
                {
                    gameBoard.player2.AbilitySlots = new CascadeContainer((int)x, (int)y, r, s, cardWidth);
                    gameBoard.player2.AbilitySlots.Type = ContainerType.AbilitySlots;
                }
                else if (name == "Player1Aura")
                {
                    gameBoard.player1.AuraZone = new CascadeContainer((int)x, (int)y, r, s, cardWidth);
                    gameBoard.player1.AuraZone.Type = ContainerType.AuraZone;
                }
                else if (name == "Player1TotalAura")
                {
                    SpriteFont font = ResourceLoader.Load<SpriteFont>("Arial");
                    gameBoard.player1.AuraPoolCounter = new AuraCounter(gameBoard.player1.AuraZone, font);
                    gameBoard.player1.AuraPoolCounter.Transforms.Position = new Vector3(x, y, 0);
                }
                else if (name == "Player2Aura")
                {
                    gameBoard.player2.AuraZone = new CascadeContainer((int)x, (int)y, r, s, cardWidth);
                    gameBoard.player2.AuraZone.Type = ContainerType.AuraZone;
                }
                else if (name == "Player2TotalAura")
                {
                    SpriteFont font = ResourceLoader.Load<SpriteFont>("Arial");
                    gameBoard.player2.AuraPoolCounter = new AuraCounter(gameBoard.player2.AuraZone, font);
                    gameBoard.player2.AuraPoolCounter.Transforms.Position = new Vector3(x, y, 0);
                }
                else if (name == "Player1Equipment")
                {
                    gameBoard.player1.EquipmentSlots = new CascadeContainer((int)x, (int)y, r, s, cardWidth);
                    gameBoard.player1.EquipmentSlots.Type = ContainerType.EquipmentSlots;
                }
                else if (name == "Player2Equipment")
                {
                    gameBoard.player2.EquipmentSlots = new CascadeContainer((int)x, (int)y, r, s, cardWidth);
                    gameBoard.player2.EquipmentSlots.Type = ContainerType.EquipmentSlots;
                }
                else if (name == "DisplayCard")
                {
                    gameBoard.displayCard = new CardSlot(cardLayout, null);
                }
            }
        }
        public static GameBoard LoadGameBoard(string fileName, IPlayer player1, DeckSet player1DeckSet, IPlayer player2, DeckSet player2DeckSet)
        {
            BoardLoader boardLoader = new BoardLoader() { player1 = player1, player1DeckSet = player1DeckSet,
                player2 = player2, player2DeckSet = player2DeckSet};

            GameWorldMap map = WorldMapLoader.LoadMap(fileName, boardLoader);
            var gameboard = (from val in map.Objects where val.Name == GameBoard_Name select val).FirstOrDefault();
            return (GameBoard)gameboard;
        }
    }
}
