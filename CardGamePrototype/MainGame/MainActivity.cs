﻿using MonoMysterionEngine;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace CardGamePrototype
{
    public class MainActivity : Activity
    {
        private GameRunner gameRunner;
        public override void Start()
        {
            MysterionEngineGame.Game.IsMouseVisible = true;
            gameRunner = new GameRunner();
            gameRunner.GameStart();
        }
        public override void Update(GameTime gameTime)
        {
            gameRunner.GameUpdate();
        }
        public override void InputUpdate(KeyboardState keyboardState, MouseState mouseState)
        {
            gameRunner.InputUpdate(keyboardState, mouseState);
        }
        public override void Destroy()
        {
            gameRunner.GameEnd();
        }
        
    }
}
