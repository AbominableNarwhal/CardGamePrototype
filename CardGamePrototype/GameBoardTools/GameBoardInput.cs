﻿using CardGamePrototype.Cards;
using Microsoft.Xna.Framework.Input;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Objects;
using System;

namespace CardGamePrototype.GameBoardTools
{
    public enum CardInputEventType { MouseEnter, MouseExit, MouseClicked, None };
    public class GameBoardInput : GameObject
    {
        private readonly BasicInputComponent inputComponent;
        private MouseState oldMouseState;
        private CardObject oldEventCard;
        private ICardContainer oldEventContainer;
        private GameBoard gameBoard;
        public event EventHandler<GameBoardMouseEventArgs> GameboardInputEvent;

        public GameBoardInput(GameBoard gameBoard)
        {
            inputComponent = new BasicInputComponent(OnInputUpdate);
            AddComponent(inputComponent);
            this.gameBoard = gameBoard;
            oldMouseState = new MouseState();
        }

        public void OnInputUpdate(KeyboardState keyboardState, MouseState mouseState)
        {
            CardObject eventCard = null;
            ICardContainer eventContainer = null;

            bool cardClicked = mouseState.LeftButton == ButtonState.Released &&
                oldMouseState.LeftButton == ButtonState.Pressed;

            foreach (ICardContainer container in gameBoard.GetContainerList())
            {
                eventCard = container.CardAtPoint(mouseState.X, mouseState.Y);
                eventContainer = container;
                if (eventCard != null)
                {
                    break;
                }
            }

            if (oldEventCard != eventCard)
            {
                if (oldEventCard != null)
                {
                    GameBoardMouseEventArgs args = new GameBoardMouseEventArgs
                    { Subject = oldEventCard, EventType = CardInputEventType.MouseExit, Location = oldEventContainer.Type };
                    GameboardInputEvent?.Invoke(gameBoard, args);
                }
                if (eventCard != null)
                {
                    GameBoardMouseEventArgs args = new GameBoardMouseEventArgs
                    { Subject = eventCard, EventType = CardInputEventType.MouseEnter, Location = eventContainer.Type };
                    GameboardInputEvent?.Invoke(gameBoard, args);
                }
            }
            if (cardClicked && eventCard != null)
            {
                GameBoardMouseEventArgs args = new GameBoardMouseEventArgs
                { Subject = eventCard, EventType = CardInputEventType.MouseClicked, Location = eventContainer.Type };
                GameboardInputEvent?.Invoke(gameBoard, args);
            }
            oldMouseState = mouseState;
            oldEventCard = eventCard;
            oldEventContainer = eventContainer;
        }
        public class GameBoardMouseEventArgs : EventArgs
        {
            public CardObject Subject { get; set; }
            public CardInputEventType EventType { get; set; }
            public ContainerType Location { get; set; }
        }
    }
}
