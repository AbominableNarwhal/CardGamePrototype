﻿using CardGamePrototype.Cards;
using CardGamePrototype.Players;
using MonoMysterionEngine;
using MonoMysterionEngine.Objects;

namespace CardGamePrototype.GameBoardTools
{
    public class GameBoardController : GameObject
    {
        public readonly GameBoardInput gameBoardInput;
        public readonly GameBoardRenderer gameBoardRenderer;
        public GameBoard gameBoard;
        public GameBoardController()
        { }
        public GameBoardController(GameBoard gameBoard)
        {
            this.gameBoard = gameBoard;
            gameBoardRenderer = new GameBoardRenderer(gameBoard);
            gameBoardInput = new GameBoardInput(gameBoard);
        }
        public void StartGame()
        {
            MysterionEngineGame.Game.ObjectLocator.AddObject(gameBoardRenderer);
            MysterionEngineGame.Game.ObjectLocator.AddObject(gameBoardInput);
            MysterionEngineGame.Game.ObjectLocator.AddObject(gameBoard.player1.AuraPoolCounter);
            MysterionEngineGame.Game.ObjectLocator.AddObject(gameBoard.player2.AuraPoolCounter);
        }
        public void MoveCard(CardObject card, ICardContainer source, ICardContainer destination)
        {
            if(card != null)
            {
                source.RemoveCard(card);
                CardLayout cardLayout = destination.AddCard(card);
                gameBoardRenderer?.MoveCard(card, cardLayout);
            }
        }

        public PlayerCollection GetPlayerOpponentCollection(IPlayer player)
        {
            if (player == gameBoard.player1.owner)
            {
                return gameBoard.player2;
            }
            else if (player == gameBoard.player2.owner)
            {
                return gameBoard.player1;
            }
            return null;
        }

        public PlayerCollection GetPlayerCollection(IPlayer player)
        {
            if (player == gameBoard.player1.owner)
            {
                return gameBoard.player1;
            }
            else if (player == gameBoard.player2.owner)
            {
                return gameBoard.player2;
            }
            return null;
        }
        public void EndGame()
        {
            ObjectManager manager = MysterionEngineGame.Game?.ObjectLocator;
            //TODO remove gameBoardRenderer's handle on the basic render component
            manager.RemoveObject(gameBoardRenderer);
            manager.RemoveObject(gameBoardInput);
            manager.RemoveObject(gameBoard.player1.AuraPoolCounter);
            manager.RemoveObject(gameBoard.player2.AuraPoolCounter);
        }
    }
}
