﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoMysterionEngine;
using MonoMysterionEngine.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardGamePrototype.GameBoardTools
{
    public class AuraCounter : GameObject
    {
        private CascadeContainer auraContainer;
        private int bonusAura;
        private TextObject titleText;
        private TextObject auraCountText;
        public AuraCounter(CascadeContainer auraContainer, SpriteFont font)
        {
            this.auraContainer = auraContainer;
            titleText = new TextObject("Total Aura", font);
            auraCountText = new TextObject("0", font);
            AddChild(titleText);
            AddChild(auraCountText);
            auraCountText.Transforms.Position = new Vector3(titleText.FontObj.Width/2, 40, 0);
        }
        public void AddBonusAura(int value)
        {
            bonusAura += value;
            UpdateText();
        }
        public int NumOfUntappedAuraCards()
        {
            int count = 0;
            foreach (CardSlot cardSlot in auraContainer.cardSlots)
            {
                if (cardSlot.card.FaceUp)
                {
                    count++;
                }
            }
            return count;
        }
        public int NumOfUntappedAura()
        {
            return NumOfUntappedAuraCards() + bonusAura;
        }
        public void TapAura(int numOfAura)
        {
            int transferAmount = Math.Min(bonusAura, numOfAura);
            bonusAura -= transferAmount;
            numOfAura -= transferAmount;
            foreach (CardSlot cardSlot in auraContainer.cardSlots)
            {
                if (numOfAura == 0)
                {
                    break;
                }
                if (cardSlot.card.FaceUp)
                {
                    numOfAura -= 1;
                    cardSlot.card.FaceUp = false;
                }
            }
            UpdateText();
        }
        public void UntapAura(int numOfAura)
        {
            List<CardSlot> cardSlots = auraContainer.cardSlots;
            for (int i = cardSlots.Count - 1; i >= 0; i--)
            {
                if (numOfAura == 0)
                {
                    break;
                }
                cardSlots[i].card.FaceUp = true;
                numOfAura -= 1;
            }
            UpdateText();
        }
        public void UpdateText()
        {
            auraCountText.FontObj.Text = NumOfUntappedAura().ToString();
        }
    }
}
