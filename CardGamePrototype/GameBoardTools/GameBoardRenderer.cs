﻿using CardGamePrototype.Cards;
using CardGamePrototype.Players;
using Microsoft.Xna.Framework.Input;
using MonoMysterionEngine;
using MonoMysterionEngine.Components;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Systems;
using System;
using System.Collections.Generic;
using static MonoMysterionEngine.Components.BasicInputComponent;
using static MonoMysterionEngine.Components.BasicRenderComponent;
using MonoMysterionEngine.Utilities;
using Microsoft.Xna.Framework;

namespace CardGamePrototype.GameBoardTools
{
    public class GameBoardRenderer : GameObject
    {
        private readonly GameBoard gameBoard;
        private Dictionary<CardObject, CardAnimator> animationMap;
        public BasicRenderComponent renderComponent;
        public GameBoardRenderer(GameBoard gameBoard)
        {
            this.gameBoard = gameBoard;
            renderComponent = new BasicRenderComponent(OnDraw);
            AddComponent(renderComponent);
            gameBoard.player1.ActiveFighter.card.SetLayout(gameBoard.player1.ActiveFighter.cardLayout);
            gameBoard.player2.ActiveFighter.card.SetLayout(gameBoard.player2.ActiveFighter.cardLayout);
            for (int i = 0; i < 2; i++)
                gameBoard.player1.SupportFighters[i].card.SetLayout(gameBoard.player1.SupportFighters[i].cardLayout);
            for (int i = 0; i < 2; i++)
                gameBoard.player2.SupportFighters[i].card.SetLayout(gameBoard.player2.SupportFighters[i].cardLayout);

            animationMap = new Dictionary<CardObject, CardAnimator>();
            //Listed for cardLayouts changing in the hand
            gameBoard.player1.Hand.CardLayoutsChangedEvent += OnHandLayoutChanged;
            gameBoard.player2.Hand.CardLayoutsChangedEvent += OnHandLayoutChanged;
        }
        public bool IsAnimating()
        {
            return animationMap.Count > 0;
        }
        public void MoveCard(CardObject card, CardLayout cardLayout)
        {
            //card.SetLayout(cardLayout);
            CardAnimator component = new CardAnimator(card, cardLayout, 100, OnAnimationComplete);
            AddAnimation(component);
        }
        public void OnHandLayoutChanged(object sender, CardLayoutChangedEventArgs e)
        {
            //e.Card.SetLayout(e.CardLayout);
            CardAnimator component = new CardAnimator(e.Card, e.CardLayout, 100, OnAnimationComplete);
            AddAnimation(component);
        }
        public void OnDraw(RenderSystem renderSystem)
        {
        }
        public void AddAnimation(CardAnimator animator)
        {
            if (animationMap.ContainsKey(animator.cardObject))
            {
                animationMap[animator.cardObject].destLayout = animator.destLayout;
            }
            else
            {
                animationMap.Add(animator.cardObject, animator);
                animator.Name += animator.destLayout.Position.ToString();
                AddComponent(animator);
            }
        }
        private void OnAnimationComplete(BasicAnimationComponent component)
        {
            CardAnimator cardAnimator = (CardAnimator)component;
            animationMap.Remove(cardAnimator.cardObject);
            RemoveComponent(component);
        }
    }
    public class CardAnimator : BasicAnimationComponent
    {
        public CardObject cardObject;
        public CardLayout destLayout;
        private CardLayout srcLayout;
        public CardAnimator(CardObject cardObject, CardLayout layout, long duration, CompleteHandler completeHandler) : base(null, completeHandler, duration)
        {
            this.cardObject = cardObject;
            destLayout = layout;
            srcLayout.Position = new Vector2(cardObject.Transforms.Position.X, cardObject.Transforms.Position.Y);
            srcLayout.Scale = cardObject.Scale;
            srcLayout.Rotation = cardObject.Rotation;
            if (cardObject.FaceUp || destLayout.FaceUp)
            {
                cardObject.FaceUp = true;
            }
            if (cardObject.IsVisable || destLayout.IsVisable)
            {
                cardObject.IsVisable = true;
            }
        }
        public override void Complete()
        {
            cardObject.FaceUp = destLayout.FaceUp;
            base.Complete();
        }
        public override bool Animate(GameTime gameTime)
        {
            CurrentTime += gameTime.ElapsedGameTime.TotalMilliseconds;
            if (CurrentTime > Duration)
            {
                CurrentTime = Duration;
            }

            //Animate Position
            cardObject.Transforms.Position = new Vector3(Vector2.Lerp(srcLayout.Position, destLayout.Position, (float)Progress), 0);

            //Animate Scale
            Vector2 srcScale = new Vector2(srcLayout.Scale, srcLayout.Scale);
            Vector2 destScale = new Vector2(destLayout.Scale, destLayout.Scale);
            cardObject.Transforms.Scale = new Vector3(Vector2.Lerp(srcScale, destScale, (float)Progress), 0);

            //Animate Rotation
            cardObject.Transforms.Rotation = MathHelper.Lerp(srcLayout.Rotation, destLayout.Rotation, (float)Progress);
            return true;
        }
    }
}
