﻿using CardGamePrototype.Cards;
using CardGamePrototype.Players;
using Microsoft.Xna.Framework;
using MonoMysterionEngine;
using MonoMysterionEngine.Objects;
using MonoMysterionEngine.Utilities;
using System;
using System.Collections.Generic;

namespace CardGamePrototype.GameBoardTools
{
    public enum ContainerType { Hand, Deck, ActiveFighter, Discard, SupportFighter,
        Played, AbilitySlots, EquipmentSlots, AuraZone, Display }
    public class GameBoard : IObjectGroup
    {
        private List<ICardContainer> containers;
        public PlayerCollection player1;
        public PlayerCollection player2;
        public CardSlot displayCard;

        public string Name { get; set; }
        public long Id { get; set; }

        public GameBoard(IPlayer p1, IPlayer p2)
        {
            containers = new List<ICardContainer>();
            player1 = new PlayerCollection(p1);
            player2 = new PlayerCollection(p2);
        }
        public List<ICardContainer> GetContainerList()
        {
            if (containers.Count == 0)
            {
                containers.Add(player1.ActiveFighter);
                containers.Add(player1.Hand);
                containers.Add(player1.Deck);
                containers.Add(player1.Discard);
                containers.Add(player1.SupportFighters[0]);
                containers.Add(player1.SupportFighters[1]);
                containers.Add(player1.PlayedCard);
                containers.Add(player1.AbilitySlots);
                containers.Add(player1.AuraZone);
                containers.Add(player1.EquipmentSlots);

                containers.Add(player2.ActiveFighter);
                containers.Add(player2.Hand);
                containers.Add(player2.Deck);
                containers.Add(player2.Discard);
                containers.Add(player2.SupportFighters[0]);
                containers.Add(player2.SupportFighters[1]);
                containers.Add(player2.PlayedCard);
                containers.Add(player2.AbilitySlots);
                containers.Add(player2.AuraZone);
                containers.Add(player2.EquipmentSlots);
            }
            return containers;
        }
    }
    public class PlayerCollection
    {
        public IPlayer owner;
        public PlayerDeckContainer Deck;
        public PlayerHandContainer Hand;
        public PlayerDeckContainer Discard;
        public SingleCardContainer ActiveFighter;
        public SingleCardContainer[] SupportFighters;
        public CascadeContainer PlayedCard;
        public CascadeContainer AbilitySlots;
        public CascadeContainer EquipmentSlots;
        public CascadeContainer AuraZone;
        public AuraCounter AuraPoolCounter;
        public PlayerCollection(IPlayer _owner)
        {
            Deck = new PlayerDeckContainer();
            Hand = new PlayerHandContainer();
            Discard = new PlayerDeckContainer();
            SupportFighters = new SingleCardContainer[2];
            owner = _owner;
        }
    }
    public class DeckSet
    {
        public DeckSet()
        {
            Deck = new List<CardObject>();
            Fighters = new CardObject[3];
            Owner = null;
        }
        public DeckSet(IPlayer player)
        {
            Deck = new List<CardObject>();
            Fighters = new CardObject[3];
            Owner = player;
        }
        public List<CardObject> Deck { get; set; }
        public CardObject[] Fighters { get; set; }
        public IPlayer Owner { get; set; }
    }
    
    public interface ICardContainer
    {
        void RemoveCard(CardObject card);
        CardLayout AddCard(CardObject card);
        bool ContainsCard(CardObject card);
        CardObject CardAtPoint(int x, int y);
        ContainerType Type { get; set; }
    }

    public class CascadeContainer : ICardContainer
    {
        public CascadeContainer() : this(0, 0, 0, 1, 0)
        {}

        public CascadeContainer(int x, int y, float rotation, float scale, float cardImageWidth, bool faceUp = true,
            int offset = 0, bool expandRight = true)
        {
            Position = new Vector2(x, y);
            Rotation = rotation;
            Scale = scale;
            expandDir = expandRight ? new Vector2(1, 0) : new Vector2(-1, 0);
            cardSlots = new List<CardSlot>();
            cardWidth = cardImageWidth * scale;
            FaceUp = faceUp;
            this.offest = offset;
        }

        public event EventHandler<CardLayoutChangedEventArgs> CardLayoutsChangedEvent;
        private float cardWidth;
        private Vector2 expandDir;
        private int offest;
        public List<CardSlot> cardSlots;
        public float Rotation { get; set; }
        public float Scale { get; set; }
        public Vector2 Position { get; set; }
        public bool FaceUp { get; set; }
        public ContainerType Type { get; set; }

        public void RemoveCard(CardObject card)
        {
            for (int i = cardSlots.Count - 1; i >= 0; i--)
            {
                if (cardSlots[i].card == card)
                {
                    cardSlots.RemoveAt(i);
                    break;
                }
            }
            int c = 0;
            foreach (CardSlot slot in cardSlots)
            {
                CardLayout cardLayout = UpdateCardLayout(c);
                CardLayoutChangedEventArgs args = new CardLayoutChangedEventArgs(slot.card, cardLayout);
                CardLayoutsChangedEvent?.Invoke(this, args);
                c++;
            }
        }

        public CardLayout AddCard(CardObject card)
        {
            cardSlots.Add(new CardSlot(new CardLayout(), card));
            CardLayout cardLayout = UpdateCardLayout(cardSlots.Count - 1);
            cardSlots[cardSlots.Count - 1].cardLayout = cardLayout;
            return cardLayout;
        }
        private CardLayout UpdateCardLayout(int index)
        {
            CardObject card = cardSlots[index].card;
            Vector2 newDir = Engine.RotateVector(expandDir, Rotation);
            Vector2 newPos = Position + newDir * index * (cardWidth - offest);
            return new CardLayout { Position = newPos, Rotation = Rotation, Scale = Scale, FaceUp = FaceUp };
        }

        public bool ContainsCard(CardObject card)
        {
            foreach (CardSlot slot in cardSlots)
            {
                if (slot.card == card)
                {
                    return true;
                }
            }
            return false;
        }

        public CardObject CardAtPoint(int x, int y)
        {
            for(int i = cardSlots.Count - 1; i >= 0; i--)
            {
                CardSlot slot = cardSlots[i];
                if (slot.card.ContainsPoint(x, y))
                {
                    return slot.card;
                }
            }
            return null;
        }
    }

    public class PlayerDeckContainer : ICardContainer
    {
        public PlayerDeckContainer() : this(new CardLayout())
        {}
        public PlayerDeckContainer(CardLayout cardLayout)
        {
            Deck = new List<CardObject>();
            Layout = cardLayout;
        }
        public CardLayout Layout { get; }
        public List<CardObject> Deck { get; }
        public CardObject GetTopCard()
        {
            if (Deck.Count == 0)
            {
                return null;
            }
            return Deck[Deck.Count - 1];
        }
        public void RemoveCard(CardObject card)
        {
            Deck.Remove(card);
        }
        public CardLayout AddCard(CardObject card)
        {
            Deck.Add(card);
            return Layout;
        }
        /// <summary>
        /// Adds a collection of cards to this container.
        /// It will automatically apply the card layout
        /// to each card.
        /// </summary>
        /// <param name="cards"> A collection of cards to add.</param>
        public void AddRange(IEnumerable<CardObject> cards)
        {
            foreach (CardObject card in cards)
            {
                Deck.Add(card);
                card.SetLayout(Layout);
            }
        }
        public bool ContainsCard(CardObject card)
        {
            return Deck.Contains(card);
        }
        public CardObject CardAtPoint(int x, int y)
        {
            CardObject card = GetTopCard();
            if (card != null && card.ContainsPoint(x, y))
            {
                return card;
            }
            return null;
        }
        public ContainerType Type { get; set; }
    }
    public class PlayerHandContainer : ICardContainer
    {
        public PlayerHandContainer() : this(new Vector2(), 0 , 0, 1, 0)
        {}
        public PlayerHandContainer(Vector2 position, int width, int height, float scale, float cardImageWidth, bool faceUp = true, bool visable = true)
        {
            Position = position; //Postion of the hand's bounding box
            Width = width; //Width of the hand's bounding box
            Height = height; //Height of the hand's bounding box
            Scale = scale; //Scale that should be appied to the cards in the hand
            Hand = new List<CardSlot>();
            cardWidth = cardImageWidth * scale;
            FaceUp = faceUp;
            IsVisable = visable;
        }
        private readonly float cardWidth;
        public int Height { get; }
        public int Width { get; }
        public float Scale { get; }
        public Vector2 Position { get; }
        public bool FaceUp { get; }
        public bool IsVisable { get; }
        public event EventHandler<CardLayoutChangedEventArgs> CardLayoutsChangedEvent;
        public List<CardSlot> Hand
        {
            get; set;
        }
        public void RemoveCard(CardObject card)
        {
            for (int i = Hand.Count - 1; i >= 0; i--)
            {
                if (Hand[i].card == card)
                {
                    Hand.RemoveAt(i);
                    break;
                }
            }
            float maxHalfWidth = Width / 2;
            float currentHalfWidth = Hand.Count * cardWidth / 2;

            ReCenterCards(maxHalfWidth, currentHalfWidth);
        }
        public CardLayout AddCard(CardObject card)
        {
            float maxHalfWidth = Width / 2;
            float currentHalfWidth = (Hand.Count + 1) * cardWidth / 2;

            ReCenterCards(maxHalfWidth, currentHalfWidth);

            CardLayout layout = new CardLayout()
            {
                Position = new Vector2(this.Position.X + cardWidth * Hand.Count + maxHalfWidth - currentHalfWidth, this.Position.Y),
                Scale = this.Scale,
                Rotation = 0,
                FaceUp = this.FaceUp,
                IsVisable = IsVisable
            };
            Hand.Add(new CardSlot(layout, card));
            return layout;
        }
        private void ReCenterCards(float maxHalfWidth, float currentHalfWidth)
        {
            for (int i = 0; i < Hand.Count; i++)
            {
                Hand[i].cardLayout = new CardLayout()
                {
                    Position = new Vector2(this.Position.X + cardWidth * i + maxHalfWidth - currentHalfWidth, this.Position.Y),
                    Scale = this.Scale,
                    Rotation = 0,
                    FaceUp = this.FaceUp,
                    IsVisable = IsVisable
                };
                CardLayoutChangedEventArgs args = new CardLayoutChangedEventArgs(Hand[i].card, Hand[i].cardLayout);
                CardLayoutsChangedEvent?.Invoke(this, args);
            }
        }
        public bool ContainsCard(CardObject card)
        {
            foreach (CardSlot slot in Hand)
            {
                if (slot.card == card)
                {
                    return true;
                }
            }
            return false;
        }   
        public CardObject CardAtPoint(int x, int y)
        {
            foreach (CardSlot slot in Hand)
            {
                if (slot.card.ContainsPoint(x, y))
                {
                    return slot.card;
                }
            }
            return null;
        }
        public ContainerType Type { get; set; }
    }

    public class CardLayoutChangedEventArgs
    {
        public CardLayoutChangedEventArgs(CardObject card, CardLayout cardLayout)
        {
            Card = card;
            CardLayout = cardLayout;
        }
        public CardObject Card { get; set; }
        public CardLayout CardLayout { get; set; }
    }

    public class SingleCardContainer : ICardContainer
    {
        public CardLayout cardLayout;
        public CardObject card;
        public SingleCardContainer() : this(new CardLayout(), null)
        {
        }
        public SingleCardContainer(CardLayout cardLayout, CardObject card)
        {
            this.cardLayout = cardLayout;
            this.card = card;
        }
        public void RemoveCard(CardObject card)
        {
            if (this.card == card)
            {
                this.card = null;
            }
        }
        public CardLayout AddCard(CardObject card)
        {
            this.card = card;
            return cardLayout;
        }
        public bool ContainsCard(CardObject card)
        {
            return this.card == card;
        }
        public CardObject CardAtPoint(int x, int y)
        {
            if(card != null && card.ContainsPoint(x, y))
            {
                return card;
            }
            return null;
        }
        public ContainerType Type { get; set; }
    }
    public class CardSlot
    {
        public CardLayout cardLayout;
        public CardObject card;
        public CardSlot()
        {
            cardLayout = new CardLayout();
            card = new CardObject();
        }
        public CardSlot(CardLayout cardLayout, CardObject card)
        {
            this.cardLayout = cardLayout;
            this.card = card;
        }
    }
    public struct CardLayout
    {
        public Vector2 Position { get; set; }
        public float Scale { get; set; }
        public float Rotation { get; set; }
        public bool IsVisable { get; set; }
        public bool FaceUp { get; set; }
    }
}
