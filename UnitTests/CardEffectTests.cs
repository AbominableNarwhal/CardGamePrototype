﻿using System;
using CardGamePrototype.Cards;
using CardGamePrototype.Players;
using CardGamePrototype.GameBoardTools;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;
using static CardGamePrototype.GameRunner;
using System.Collections.Generic;
using CardGamePrototype.MainGame.Actions;
using CardGamePrototype.Cards.CardEffects;

namespace UnitTests
{
    [TestClass]
    public class CardEffectTests
    {
        private static GameRunnerData GameData;
        public static GameRunnerData GetGameData()
        {
            HumanPlayer player1 = new HumanPlayer();
            HumanPlayer player2 = new HumanPlayer();
            FighterCard p1ActiveFighter = new FighterCard(600, 100, 3, 200, 2, 6, "", player1);
            FighterCard p2ActiveFighter = new FighterCard(800, 300, 3, 150, 2, 6, "", player2);
            GameBoard gameBoard = new GameBoard(player1, player2);
            gameBoard.player1.PlayedCard = new CascadeContainer();
            gameBoard.player1.ActiveFighter = new SingleCardContainer(new CardLayout(), p1ActiveFighter);
            gameBoard.player1.SupportFighters[0] = new SingleCardContainer(new CardLayout(), new FighterCard(600, 100, 3, 200, 2, 6, "", player1));
            gameBoard.player1.Hand.AddCard(new PowerCard(2, 7, 3, player1));
            gameBoard.player1.Hand.AddCard(new AbilityCard(player1));
            gameBoard.player1.Hand.AddCard(new AuraCard(player1));
            gameBoard.player1.Hand.AddCard(new EquipmentCard("", player1));
            gameBoard.player1.Hand.AddCard(new ItemCard(player1));
            gameBoard.player1.Hand.AddCard(new AbilityCard(player1));
            gameBoard.player1.Hand.AddCard(new EquipmentCard("", player1));
            gameBoard.player1.AbilitySlots = new CascadeContainer();
            Ability ability = new Ability(100);
            gameBoard.player1.AbilitySlots.AddCard(new AbilityCard(player1) { Ability = ability });
            gameBoard.player2.PlayedCard = new CascadeContainer();
            gameBoard.player2.ActiveFighter = new SingleCardContainer(new CardLayout(), p1ActiveFighter);
            gameBoard.player2.Hand.AddCard(new PowerCard(2, 7, 3, player2));
            gameBoard.player2.Hand.AddCard(new AbilityCard(player2));
            gameBoard.player2.AbilitySlots = new CascadeContainer();
            gameBoard.player2.AbilitySlots.AddCard(new AbilityCard(player2));
            GameData = new GameRunnerData();
            GameData.gameBoardController = new GameBoardController();
            GameData.gameBoardController.gameBoard = gameBoard;
            GameData.player1 = player1;
            GameData.player2 = player2;

            return GameData;
        }

        [TestMethod]
        public void TestMagicalStrength()
        {

            GameRunnerData gameData = GetGameData();
            PowerCard powerCard = new PowerCard(0, 0, 0, gameData.player1, new Magical_Strength());
            ICardEffect cardEffect = powerCard.PowerCardEffect;
            CascadeContainer playedCardZone = gameData.gameBoardController.gameBoard.player1.PlayedCard;
            playedCardZone.AddCard(powerCard);
            //Fighter stats before card effect activates:
            //Magic: 100, Strength: 200
            IEnumerable<IAction> actions = cardEffect.OnActivate(gameData);
            foreach (IAction action in actions)
            {
                action.Execute();
            }

            //Should stay in play
            Assert.AreNotEqual(0, playedCardZone.cardSlots.Count);

            FighterCard fighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player1.ActiveFighter.card;

            //Fighter stats after card effect activates:
            //Magic: 200, Strength: 100
            //stats should be swapped
            Assert.AreEqual(100, fighterCard.Strength);
            Assert.AreEqual(200, fighterCard.Magic);

            fighterCard.Strength = 50;

            Assert.AreEqual(100, fighterCard.Strength);
            Assert.AreEqual(50, fighterCard.Magic);

            gameData.AddActions(cardEffect.OnTurnEnd(gameData));
            while (gameData.ProcessCardEffects())
            { }

            PlayerDeckContainer discardZone = gameData.gameBoardController.gameBoard.player1.Discard;

            //The Card should be removed from play
            Assert.AreEqual(0, playedCardZone.cardSlots.Count);
            Assert.IsTrue(discardZone.ContainsCard(powerCard));
            //And fighter stats should be changed back
            Assert.AreEqual(50, fighterCard.Strength);
            //Assert.AreEqual(50, fighterCard.Magic);

        }

        [TestMethod]
        public void TestGuardiansPunishment()
        {
            GameRunnerData gameData = GetGameData();
            FighterCard player1FighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player1.ActiveFighter.card;
            FighterCard player2FighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player2.ActiveFighter.card;
            player2FighterCard.HP = 150;
            AbilityCard abilityCard = new AbilityCard(gameData.player1);
            Guardians_Punishment guardians_Punishment = new Guardians_Punishment();

            abilityCard.SetAbilityStats(100, 0, 0, 200, 0, 0, 0, "", 1, "", guardians_Punishment);
            player1FighterCard.AttachAbilityCard(abilityCard);

            ICardEffect cardEffect = guardians_Punishment;

            IEnumerable<IAction> actions = cardEffect.OnActivate(gameData);
            foreach (IAction action in actions)
            {
                action.Execute();
            }

            int tenthStrength = player1FighterCard.Strength / 100;

            //Guardians punishment should do 100 plus 1/10 * the user's strength
            Assert.AreEqual(150 - (100 + tenthStrength), player2FighterCard.HP);
        }
    }
}
