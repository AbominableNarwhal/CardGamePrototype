﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CardGamePrototype.MainGame.Actions;
using CardGamePrototype.Cards;
using static CardGamePrototype.GameRunner;
using CardGamePrototype.Players;
using CardGamePrototype.GameBoardTools;

namespace UnitTests
{
    [TestClass]
    public class ActionsTests
    {
        [TestMethod]
        public void TestFighterStatSwap()
        {
            //Test default contructor
            FighterStatSwapAction fighterStatSwapAction = new FighterStatSwapAction();
            FighterCard fighterCard = new FighterCard(20, 60, 30, 40, 2, 3, "");
            fighterStatSwapAction.Stat1 = FighterStats.Magic;
            fighterStatSwapAction.Stat2 = FighterStats.Strength;
            fighterStatSwapAction.Fighter = fighterCard;

            IAction action = fighterStatSwapAction;
            action.Execute();

            Assert.AreEqual(60, fighterCard.Strength);
            Assert.AreEqual(40, fighterCard.Magic);

            fighterCard.Strength = 100;

            Assert.AreEqual(60, fighterCard.Strength);
            Assert.AreEqual(100, fighterCard.Magic);

            action.Execute();

            Assert.AreEqual(100, fighterCard.Strength);
            Assert.AreEqual(60, fighterCard.Magic);

            fighterStatSwapAction.Stat1 = FighterStats.Strength;
            fighterStatSwapAction.Stat2 = FighterStats.Strength;
            action.Execute();

            Assert.AreEqual(100, fighterCard.Strength);
            Assert.AreEqual(60, fighterCard.Magic);

            //Test error handling
            fighterStatSwapAction.Fighter = null;
            action.Execute();

            Assert.AreEqual(100, fighterCard.Strength);
            Assert.AreEqual(60, fighterCard.Magic);

            fighterStatSwapAction = new FighterStatSwapAction();
            fighterStatSwapAction.Fighter = fighterCard;
            action = fighterStatSwapAction;
            action.Execute();

            //Test Contructor
            fighterStatSwapAction = new FighterStatSwapAction(FighterStats.Strength, FighterStats.Tech, fighterCard);

            Assert.AreEqual(FighterStats.Strength, fighterStatSwapAction.Stat1);
            Assert.AreEqual(FighterStats.Tech, fighterStatSwapAction.Stat2);
            Assert.AreSame(fighterCard, fighterStatSwapAction.Fighter);

            action = fighterStatSwapAction;
            action.Execute();

            Assert.AreEqual(30, fighterCard.Strength);
            Assert.AreEqual(100, fighterCard.Tech);
        }
        [TestMethod]
        public void TestEndPlayedCardAction()
        {
            //Test default contructor
            EndPlayedCardEffectAction endPlayedCardAction = new EndPlayedCardEffectAction();
            GameRunnerData gameData = CardEffectTests.GetGameData();
            IPlayer player = gameData.player1;
            PowerCard powerCard = new PowerCard(0, 0, 0, player, null);
            CascadeContainer playedCardZone = gameData.gameBoardController.gameBoard.player1.PlayedCard;
            playedCardZone.AddCard(powerCard);

            PlayerDeckContainer discardZone = gameData.gameBoardController.gameBoard.player1.Discard;
            endPlayedCardAction.TargetCard = powerCard;
            endPlayedCardAction.GameData = gameData;
            IAction action = endPlayedCardAction;
            action.Execute();

            Assert.AreEqual(0, playedCardZone.cardSlots.Count);
            Assert.IsTrue(discardZone.ContainsCard(powerCard));

            //Test with bad parameters
            powerCard = new PowerCard(0, 0, 0, player, null);
            endPlayedCardAction = new EndPlayedCardEffectAction() {TargetCard = null, GameData = null };
            playedCardZone.AddCard(powerCard);
            //nothing should change
            action = endPlayedCardAction;
            action.Execute();

            Assert.AreSame(powerCard, playedCardZone.cardSlots[0].card);
            Assert.IsFalse(discardZone.ContainsCard(powerCard));

            //What if the played card zone is empty?
            playedCardZone.RemoveCard(powerCard);
            endPlayedCardAction.TargetCard = powerCard;
            endPlayedCardAction.GameData = gameData;

            action.Execute();

            //also nothing should happen
            Assert.AreEqual(0, playedCardZone.cardSlots.Count);
            Assert.IsFalse(discardZone.ContainsCard(powerCard));
        }

        [TestMethod]
        public void TestDamageAction()
        {
            DamageAction damageAction = new DamageAction();
            GameRunnerData gameData = CardEffectTests.GetGameData();
            FighterCard fighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player1.ActiveFighter.card;
            IAction action = damageAction;

            fighterCard.HP = 100;
            damageAction.TargetFighter = fighterCard;
            damageAction.Damage = 50;

            action.Execute();

            Assert.AreEqual(50, fighterCard.HP);
        }
    }
}
