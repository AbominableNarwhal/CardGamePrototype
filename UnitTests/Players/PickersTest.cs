﻿using System;
using CardGamePrototype.Cards;
using CardGamePrototype.Players;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static CardGamePrototype.GameRunner;

namespace UnitTests.Players
{
    [TestClass]
    public class PickersTest
    {
        [TestMethod]
        public void FieldPickerTest()
        {
            CardObject[] cardList = new CardObject[] { new CardObject(), new CardObject(), new CardObject(), new CardObject() };
            CardObject pickedCard = null;
            CardPicker cardPicker = new CardPicker();
            cardPicker.PickFromField = true;
            cardPicker.SetCardOptions(cardList);
            cardPicker.OnCardPickedEvent += delegate (object sender, CardPicker.CardPickedEventArgs e)
            {
                pickedCard = e.PickedCard;
            };
            cardPicker.OnCardInHandClicked(cardList[0]);

            Assert.AreSame(pickedCard, cardList[0]);
        }
        [TestMethod]
        public void CombatInputPlayPowerTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            CombatInputState.CombatInputEventArgs eventArgs = null;
            CombatInputState combatInputState = new CombatInputState(new CombatQuery(), gameData.player1, gameData.gameBoardController.gameBoard,
                delegate (object sender, CombatInputState.CombatInputEventArgs e)
                {
                    eventArgs = e;
                });
            IInputState inputState = combatInputState;

            //Picking power card from hand
            CardObject card = gameData.gameBoardController.gameBoard.player1.Hand.Hand[0].card;
            inputState.OnCardInHandClicked(card);

            Assert.IsNotNull(eventArgs);
            Assert.AreEqual(QueryAnswerType.PlayPowerCard, eventArgs.Answer.Type);
            Assert.AreSame(card, eventArgs.Answer.Card);

        }
        [TestMethod]
        public void CombatInputPlayItemTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            CombatInputState.CombatInputEventArgs eventArgs = null;
            CombatInputState combatInputState = new CombatInputState(new CombatQuery(), gameData.player1, gameData.gameBoardController.gameBoard,
                delegate (object sender, CombatInputState.CombatInputEventArgs e)
                {
                    eventArgs = e;
                });
            IInputState inputState = combatInputState;

            //Picking item card from hand
            CardObject card = gameData.gameBoardController.gameBoard.player1.Hand.Hand[4].card;
            inputState.OnCardInHandClicked(card);

            Assert.IsNotNull(eventArgs);
            Assert.AreEqual(QueryAnswerType.PlayItemCard, eventArgs.Answer.Type);
            Assert.AreSame(card, eventArgs.Answer.Card);

        }
        [TestMethod]
        public void CombatInputAttachAbilityTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            CombatInputState.CombatInputEventArgs eventArgs = null;
            CombatInputState combatInputState = new CombatInputState(new CombatQuery(), gameData.player1, gameData.gameBoardController.gameBoard,
                delegate (object sender, CombatInputState.CombatInputEventArgs e)
                {
                    eventArgs = e;
                });
            IInputState inputState = combatInputState;

            //Picking ability card from hand
            AbilityCard abilityCard = (AbilityCard)gameData.gameBoardController.gameBoard.player1.Hand.Hand[1].card;
            inputState.OnCardInHandClicked(abilityCard);

            Assert.IsNull(eventArgs);

            //Picking active fighter to attach the ability to
            FighterCard fighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player1.ActiveFighter.card;
            inputState.OnActiveFighterClicked(fighterCard);

            Assert.AreEqual(QueryAnswerType.AttachAbilityCard, eventArgs.Answer.Type);
            Assert.AreSame(abilityCard, eventArgs.Answer.Card);
            Assert.AreSame(fighterCard, eventArgs.Answer.TargetFighter);

            //Now pick support fighter to attach an ability to
            eventArgs = null;
            abilityCard = (AbilityCard)gameData.gameBoardController.gameBoard.player1.Hand.Hand[5].card;
            inputState.OnCardInHandClicked(abilityCard);

            Assert.IsNull(eventArgs);

            fighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player1.SupportFighters[0].card;
            inputState.OnSupportFighterClicked(fighterCard);

            Assert.IsNotNull(eventArgs);
            Assert.AreEqual(QueryAnswerType.AttachAbilityCard, eventArgs.Answer.Type);
            Assert.AreSame(abilityCard, eventArgs.Answer.Card);
            Assert.AreSame(fighterCard, eventArgs.Answer.TargetFighter);
        }
        [TestMethod]
        public void CombatInputBugsTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            CombatInputState.CombatInputEventArgs eventArgs = null;
            CombatInputState combatInputState = new CombatInputState(new CombatQuery(), gameData.player1, gameData.gameBoardController.gameBoard,
                delegate (object sender, CombatInputState.CombatInputEventArgs e)
                {
                    eventArgs = e;
                });
            IInputState inputState = combatInputState;

            //Selecting a support fighter first
            FighterCard fighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player1.SupportFighters[0].card;
            inputState.OnSupportFighterClicked(fighterCard);

            Assert.IsNull(eventArgs);
        }
        [TestMethod]
        public void CombatInputAttachEquipmentTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            CombatInputState.CombatInputEventArgs eventArgs = null;
            CombatInputState combatInputState = new CombatInputState(new CombatQuery(), gameData.player1, gameData.gameBoardController.gameBoard,
                delegate (object sender, CombatInputState.CombatInputEventArgs e)
                {
                    eventArgs = e;
                });
            IInputState inputState = combatInputState;

            //Pick an equipment card from the player's hand
            EquipmentCard equipmentCard = (EquipmentCard)gameData.gameBoardController.gameBoard.player1.Hand.Hand[3].card;
            inputState.OnCardInHandClicked(equipmentCard);

            Assert.IsNull(eventArgs);

            //Picking active fighter to attach the equipment to
            FighterCard fighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player1.ActiveFighter.card;
            inputState.OnActiveFighterClicked(fighterCard);

            Assert.IsNotNull(eventArgs);
            Assert.AreEqual(QueryAnswerType.AttachEquipmentCard, eventArgs.Answer.Type);
            Assert.AreSame(equipmentCard, eventArgs.Answer.Card);
            Assert.AreSame(fighterCard, eventArgs.Answer.TargetFighter);

            //Now pick support fighter to attach equipment to
            eventArgs = null;
            equipmentCard = (EquipmentCard)gameData.gameBoardController.gameBoard.player1.Hand.Hand[6].card;
            inputState.OnCardInHandClicked(equipmentCard);

            Assert.IsNull(eventArgs);

            fighterCard = (FighterCard)gameData.gameBoardController.gameBoard.player1.SupportFighters[0].card;
            inputState.OnSupportFighterClicked(fighterCard);

            Assert.IsNotNull(eventArgs);
            Assert.AreEqual(QueryAnswerType.AttachEquipmentCard, eventArgs.Answer.Type);
            Assert.AreSame(equipmentCard, eventArgs.Answer.Card);
            Assert.AreSame(fighterCard, eventArgs.Answer.TargetFighter);
        }
        [TestMethod]
        public void CombatInputUseAbilityTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            CombatInputState.CombatInputEventArgs eventArgs = null;
            CombatInputState combatInputState = new CombatInputState(new CombatQuery(), gameData.player1, gameData.gameBoardController.gameBoard,
                delegate (object sender, CombatInputState.CombatInputEventArgs e)
                {
                    eventArgs = e;
                });
            IInputState inputState = combatInputState;

            //Double click ability card to activate it
            AbilityCard abilityCard = (AbilityCard)gameData.gameBoardController.gameBoard.player1.AbilitySlots.cardSlots[0].card;

            inputState.OnAttachedAblilityCardClicked(abilityCard);

            Assert.IsNull(eventArgs);

            inputState.OnAttachedAblilityCardClicked(abilityCard);

            Assert.AreEqual(QueryAnswerType.ActivateAbility, eventArgs.Answer.Type);
            Assert.AreEqual(abilityCard.Ability, eventArgs.Answer.AbilityToActivate);

            eventArgs = null;
            inputState.ResetState();
            inputState.ActivateAbilitySignal(abilityCard.Ability);

            Assert.IsNotNull(eventArgs);
            Assert.AreEqual(QueryAnswerType.ActivateAbility, eventArgs.Answer.Type);
            Assert.AreEqual(abilityCard.Ability, eventArgs.Answer.AbilityToActivate);
        }
        [TestMethod]
        public void CombatInputPlayAuraTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            CombatInputState.CombatInputEventArgs eventArgs = null;
            CombatInputState combatInputState = new CombatInputState(new CombatQuery(), gameData.player1, gameData.gameBoardController.gameBoard,
                delegate (object sender, CombatInputState.CombatInputEventArgs e)
                {
                    eventArgs = e;
                });
            IInputState inputState = combatInputState;

            AuraCard auraCard = (AuraCard)gameData.gameBoardController.gameBoard.player1.Hand.Hand[2].card;
            inputState.OnCardInHandClicked(auraCard);

            Assert.IsNotNull(eventArgs);
            Assert.AreEqual(QueryAnswerType.PlayAura, eventArgs.Answer.Type);
            Assert.AreSame(auraCard, eventArgs.Answer.Card);
        }
        [TestMethod]
        public void CombatInputEndTurnTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            CombatInputState.CombatInputEventArgs eventArgs = null;
            CombatInputState combatInputState = new CombatInputState(new CombatQuery(), gameData.player1, gameData.gameBoardController.gameBoard,
                delegate (object sender, CombatInputState.CombatInputEventArgs e)
                {
                    eventArgs = e;
                });
            IInputState inputState = combatInputState;

            inputState.OnEndTurnSignal();

            Assert.IsNotNull(eventArgs);
            Assert.AreEqual(QueryAnswerType.None, eventArgs.Answer.Type);
        }
    }
}
