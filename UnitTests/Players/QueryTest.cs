﻿using System;
using CardGamePrototype.Cards;
using CardGamePrototype.Players;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static CardGamePrototype.GameRunner;

namespace UnitTests.Players
{
    [TestClass]
    public class QueryTest
    {
        [TestMethod]
        public void CombatQueryTest()
        {
            CombatQuery query = new CombatQuery();
            Assert.AreEqual(query.Type, QueryType.ChooseCombat, "Default type mismatch");
        }
        [TestMethod]
        public void CombatQueryAndAnswerTest()
        {
            GameRunnerData gamedata = CardEffectTests.GetGameData();
            CombatQuery query = new CombatQuery();
            CombatQueryAnswer answer = new CombatQueryAnswer();

            Assert.AreEqual(QueryAnswerType.None, answer.Type, "Default type mismatch");
            Assert.IsTrue(query.IsValidAnswer(gamedata, answer), "Query answer validation failed");

            answer.Type = QueryAnswerType.PlayPowerCard;

            Assert.AreEqual(QueryAnswerType.PlayPowerCard, answer.Type);
            Assert.IsTrue(query.IsValidAnswer(gamedata, answer), "Query answer validation failed");
        }
        [TestMethod]
        public void DefensiveResponceQueryTest()
        {
            DefensiveResponceQuery defensiveResponceQuery = new DefensiveResponceQuery();
            IQuery query = defensiveResponceQuery;

            Assert.AreEqual(query.Type, QueryType.DefensiveResponce, "Default type mismatch");

            CombatQueryAnswer defensiveResponceAnswer = new CombatQueryAnswer();
            IQueryAnswer answer = defensiveResponceAnswer;
            GameRunnerData gamedata = CardEffectTests.GetGameData();

            Assert.IsTrue(query.IsValidAnswer(gamedata, answer), "Query answer validation failed");

        }
        [TestMethod]
        public void PlayerInterfaceWithQueriesTest()
        {
            GameRunnerData gamedata = CardEffectTests.GetGameData();
            HumanPlayer humanPlayer = new HumanPlayer();
            CombatQuery query = new CombatQuery();
            ComputerPlayer computerPlayer = new ComputerPlayer();

            //Check initial state
            IQueryAnswer answer = humanPlayer.GetQueryAnswer();
            Assert.AreEqual(null, answer);
            answer = computerPlayer.GetQueryAnswer();
            Assert.AreEqual(null, answer);

            computerPlayer.OnQuerySubmitted(query, gamedata);
            answer = computerPlayer.GetQueryAnswer();

            Assert.AreNotEqual(null, answer);
            Assert.AreEqual(QueryAnswerType.PlayPowerCard, answer.Type);

            //Players will only return an answer once per query
            answer = computerPlayer.GetQueryAnswer();
            Assert.AreEqual(null, answer);

            //Human players require input to validate this, so for now
            //     we'll just validate a simulated situation
            humanPlayer.OnQuerySubmitted(query, gamedata);
            answer = humanPlayer.GetQueryAnswer();

            //Still waiting for input
            Assert.AreEqual(null, answer);

            CardObject card = new CardObject();
            //Input received
            humanPlayer.OnCardInHandClicked(card);
            answer = humanPlayer.GetQueryAnswer();

            Assert.AreEqual(QueryAnswerType.PlayPowerCard, answer.Type);
            Assert.IsTrue(answer.GetType() == typeof(CombatQueryAnswer), "CombatQuery should have a Combat Query answer");

            CombatQueryAnswer combatQueryAnswer = (CombatQueryAnswer)answer;

            Assert.AreEqual(card, combatQueryAnswer.Card);

            humanPlayer.OnQuerySubmitted(query, gamedata);

            //The player should still be waiting for input
            Assert.AreEqual(null, humanPlayer.GetQueryAnswer());

            humanPlayer.OnCardInHandClicked(card);
            combatQueryAnswer = (CombatQueryAnswer)humanPlayer.GetQueryAnswer();

            Assert.AreEqual(card, combatQueryAnswer.Card);

            //The human player should not return a query answer if the query
            //     has already been answered dispite new input events
            humanPlayer.OnCardInHandClicked(card);

            Assert.AreEqual(null, humanPlayer.GetQueryAnswer());

        }
    }
}
