﻿using System;
using CardGamePrototype.Cards;
using CardGamePrototype.Players;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using static CardGamePrototype.GameRunner;

namespace UnitTests.Players
{
    [TestClass]
    public class HumanPlayerTest
    {
        [TestMethod]
        public void AttachAbilityTest()
        {
            GameRunnerData gamedata = CardEffectTests.GetGameData();
            HumanPlayer humanPlayer = (HumanPlayer)gamedata.player1;
            IPlayer player = humanPlayer;
            CombatQuery query = new CombatQuery();

            player.OnQuerySubmitted(query, gamedata);
            //The second card in Player1's hand is an ability card
            CardObject card = gamedata.gameBoardController.gameBoard.player1.Hand.Hand[1].card;
            player.OnCardInHandClicked(card);
            FighterCard fighterCard = (FighterCard)gamedata.gameBoardController.gameBoard.player1.ActiveFighter.card;
            player.OnActiveFighterClicked(fighterCard);
            CombatQueryAnswer answer = (CombatQueryAnswer)player.GetQueryAnswer();

            Assert.AreEqual(QueryAnswerType.AttachAbilityCard, answer.Type);
            Assert.AreSame(card, answer.Card);
            Assert.AreSame(fighterCard, answer.TargetFighter);

            //Try to attach an ability to a support fighter
            player.OnQuerySubmitted(query, gamedata);
            card = gamedata.gameBoardController.gameBoard.player1.Hand.Hand[5].card;
            player.OnCardInHandClicked(card);
            fighterCard = (FighterCard)gamedata.gameBoardController.gameBoard.player1.SupportFighters[0].card;
            player.OnSupportFighterClicked(fighterCard);
            answer = (CombatQueryAnswer)player.GetQueryAnswer();

            Assert.IsNotNull(answer);
            Assert.AreEqual(QueryAnswerType.AttachAbilityCard, answer.Type);
            Assert.AreSame(card, answer.Card);
            Assert.AreSame(fighterCard, answer.TargetFighter);
        }

        [TestMethod]
        public void AttachEquipmentTest()
        {
            GameRunnerData gamedata = CardEffectTests.GetGameData();
            HumanPlayer humanPlayer = (HumanPlayer)gamedata.player1;
            IPlayer player = humanPlayer;
            CombatQuery query = new CombatQuery();

            player.OnQuerySubmitted(query, gamedata);
            //The fourth card in Player1's hand is an equipment card
            CardObject card = gamedata.gameBoardController.gameBoard.player1.Hand.Hand[3].card;
            player.OnCardInHandClicked(card);
            FighterCard fighterCard = (FighterCard)gamedata.gameBoardController.gameBoard.player1.ActiveFighter.card;
            player.OnActiveFighterClicked(fighterCard);
            CombatQueryAnswer answer = (CombatQueryAnswer)player.GetQueryAnswer();

            Assert.IsNotNull(answer);
            Assert.AreEqual(QueryAnswerType.AttachEquipmentCard, answer.Type);
            Assert.AreSame(fighterCard, answer.TargetFighter);
        }

        [TestMethod]
        public void ActivateAbilityTest()
        {
            GameRunnerData gamedata = CardEffectTests.GetGameData();
            HumanPlayer humanPlayer = (HumanPlayer)gamedata.player1;
            IPlayer player = humanPlayer;
            CombatQuery query = new CombatQuery();

            player.OnQuerySubmitted(query, gamedata);
            //Simulate a double click on an attached ability card
            AbilityCard abilityCard = (AbilityCard)gamedata.gameBoardController.gameBoard.player1.AbilitySlots.cardSlots[0].card;
            player.OnAttachedAblilityCardClicked(abilityCard);

            Assert.IsNull(player.GetQueryAnswer());

            player.OnAttachedAblilityCardClicked(abilityCard);
            CombatQueryAnswer answer = (CombatQueryAnswer)player.GetQueryAnswer();

            Assert.IsNotNull(answer);
            Assert.AreEqual(QueryAnswerType.ActivateAbility, answer.Type);
            Assert.AreEqual(abilityCard.Ability, answer.AbilityToActivate);

            player.OnQuerySubmitted(query, gamedata);
            player.ActivateAbilitySignal(abilityCard.Ability);
            answer = (CombatQueryAnswer)player.GetQueryAnswer();

            Assert.IsNotNull(answer);
            Assert.AreEqual(QueryAnswerType.ActivateAbility, answer.Type);
            Assert.AreEqual(abilityCard.Ability, answer.AbilityToActivate);
        }
        [TestMethod]
        public void PlayAuraTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            HumanPlayer humanPlayer = (HumanPlayer)gameData.player1;
            IPlayer player = humanPlayer;
            CombatQuery query = new CombatQuery();
            AuraCard auraCard = (AuraCard)gameData.gameBoardController.gameBoard.player1.Hand.Hand[2].card;

            player.OnQuerySubmitted(query, gameData);
            player.OnCardInHandClicked(auraCard);
            CombatQueryAnswer answer = (CombatQueryAnswer)player.GetQueryAnswer();

            Assert.IsNotNull(answer);
            Assert.AreEqual(QueryAnswerType.PlayAura, answer.Type);
            Assert.AreEqual(auraCard, answer.Card);
        }
        [TestMethod]
        public void PlayItemTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            HumanPlayer humanPlayer = (HumanPlayer)gameData.player1;
            IPlayer player = humanPlayer;
            CombatQuery query = new CombatQuery();
            ItemCard itemCard = (ItemCard)gameData.gameBoardController.gameBoard.player1.Hand.Hand[4].card;

            player.OnQuerySubmitted(query, gameData);
            player.OnCardInHandClicked(itemCard);
            CombatQueryAnswer answer = (CombatQueryAnswer)player.GetQueryAnswer();

            Assert.IsNotNull(answer);
            Assert.AreEqual(QueryAnswerType.PlayItemCard, answer.Type);
            Assert.AreEqual(itemCard, answer.Card);
        }
        [TestMethod]
        public void EndTurnTest()
        {
            GameRunnerData gameData = CardEffectTests.GetGameData();
            HumanPlayer humanPlayer = (HumanPlayer)gameData.player1;
            IPlayer player = humanPlayer;
            CombatQuery query = new CombatQuery();

            player.OnQuerySubmitted(query, gameData);
            player.OnEndTurnSignal();
            CombatQueryAnswer answer = (CombatQueryAnswer)player.GetQueryAnswer();

            Assert.IsNotNull(answer);
            Assert.AreEqual(QueryAnswerType.None, answer.Type);
        }
    }
}
