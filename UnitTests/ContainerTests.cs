﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CardGamePrototype.Players;
using CardGamePrototype.Cards;
using CardGamePrototype.GameBoardTools;
using Microsoft.Xna.Framework;
using System;
using static CardGamePrototype.GameRunner;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class GameBoardTest
    {
        [TestMethod]
        public void CardLayoutTest()
        {
            CardLayout cardLayout = new CardLayout();

            //Test default values
            Assert.AreEqual(new CardLayout(), cardLayout);

            cardLayout = new CardLayout() { Rotation = 8, Scale = 4, Position = new Vector2(5, 8), IsVisable = true };
            Assert.AreEqual(8, cardLayout.Rotation);
            Assert.AreEqual(4, cardLayout.Scale);
            Assert.AreEqual(new Vector2(5, 8), cardLayout.Position);
        }

        [TestMethod]
        public void CardObjectTest()
        {
            CardObject cardObject = new CardObject();
            //Test default values
            Assert.AreEqual(new Vector3(0, 0, 0), cardObject.Transforms.Position);
            Assert.AreEqual(1, cardObject.Scale);
            Assert.AreEqual(0, cardObject.Rotation);

            CardLayout cardLayout = new CardLayout() { Position = new Vector2(5, 8), Rotation = 8, Scale = 4, IsVisable = true, FaceUp = false};
            cardObject.SetLayout(cardLayout);

            //test new expected values
            Assert.AreEqual(new Vector3(5, 8, 0), cardObject.Transforms.Position);
            Assert.AreEqual(4, cardObject.Scale);
            Assert.AreEqual(8, cardObject.Rotation);
            Assert.IsFalse(cardObject.FaceUp);
            Assert.IsTrue(cardObject.IsVisable);
        }
        [TestMethod]
        public void CardSlotTest()
        {
            CardSlot cardSlot = new CardSlot();

            //Test default values
            Assert.AreEqual(new CardLayout(), cardSlot.cardLayout);
            Assert.AreEqual(typeof(CardObject), cardSlot.card.GetType());

            CardObject card = new CardObject();
            CardLayout cardLayout = new CardLayout() { Position = new Vector2(5, 8), Rotation = 8, Scale = 4 };
            cardSlot = new CardSlot(cardLayout, card);

            Assert.AreEqual(cardLayout, cardSlot.cardLayout);
            Assert.AreSame(card, cardSlot.card);

        }
        [TestMethod]
        public void SingleCardContainerTest()
        {
            SingleCardContainer singleCard = new SingleCardContainer();

            //Test default values
            Assert.AreEqual(new CardLayout(), singleCard.cardLayout);
            Assert.AreEqual(null, singleCard.card);

            //Test constructor
            CardObject card = new UnitTestCardObject(50, 50);
            CardLayout cardLayout = new CardLayout() { Position = new Vector2(5, 8), Rotation = 8, Scale = 4 };
            singleCard = new SingleCardContainer(cardLayout, card);

            Assert.AreEqual(cardLayout, singleCard.cardLayout);
            Assert.AreSame(card, singleCard.card);

            //Test Card Container implementation
            ICardContainer cardContainer = singleCard;
            cardContainer.RemoveCard(card);

            //This container's capacity is only one card so removing it will make
            //the SingleCardContainer's card Object null
            Assert.AreEqual(null, singleCard.card);

            //Adding to this container will just simply assign to the SingleCardContainer's card Object
            cardContainer.AddCard(card);

            Assert.AreSame(card, singleCard.card);

            //If adding to this container while another card to occupying the only slot then the current
            //card is overwritten with the new one
            CardObject card2 = new CardObject();
            cardContainer.AddCard(card2);

            Assert.AreSame(card2, singleCard.card);

            //Removing a card that the container doesn't have does nothing
            cardContainer.RemoveCard(card);

            Assert.AreSame(card2, singleCard.card);

            //Removing the correct card will cause the only slot to be equal to null dispite the number of cards that
            //were added
            cardContainer.RemoveCard(card2);

            Assert.AreSame(null, singleCard.card);

            //Adding cards should return the CardLayout of the newly added card. Note the layout is not applied to the card
            //the caller is responsible for applying the layout
            Assert.AreEqual(cardLayout, cardContainer.AddCard(card));
            Assert.IsTrue(cardContainer.ContainsCard(card));
            Assert.IsFalse(cardContainer.ContainsCard(card2));

            //Test to see if the card container is calling back to the CardObject to check the bounds
            //card.Width = 50, card.Height = 50
            Assert.AreSame(card, cardContainer.CardAtPoint(25, 25));
            Assert.IsNull(cardContainer.CardAtPoint(51, 51));

            //Null card test
            singleCard.card = null;

            Assert.IsNull(cardContainer.CardAtPoint(25, 25));
        }
        [TestMethod]
        public void PlayerHandContainerTest()
        {
            PlayerHandContainer playerHandContainer = new PlayerHandContainer();

            //Test default values
            Assert.AreEqual(0, playerHandContainer.Hand.Count);
            Assert.AreEqual(new Vector2(0, 0), playerHandContainer.Position);
            Assert.AreEqual(0, playerHandContainer.Width);
            Assert.AreEqual(0, playerHandContainer.Height);
            Assert.AreEqual(1, playerHandContainer.Scale);

            //Test Constructor
            playerHandContainer = new PlayerHandContainer(new Vector2(10, 10), 60, 30, 1, 10);
            Assert.AreEqual(new Vector2(10, 10), playerHandContainer.Position);
            Assert.AreEqual(60, playerHandContainer.Width);
            Assert.AreEqual(30, playerHandContainer.Height);
            Assert.AreEqual(1, playerHandContainer.Scale);
            Assert.AreNotSame(null, playerHandContainer.Hand);

            //Test Card Container implementation
            ICardContainer cardContainer = playerHandContainer;

            //PlayerHandContainer contains a list of card slots. When adding a new card, a new card slot is
            //created to contain the card. The CardLayout that is returned should have cordinates that are
            //horizontally centered on the bounding box of the container.
            CardObject[] cards = new CardObject[] { new UnitTestCardObject(10, 10), new UnitTestCardObject(10, 10), new UnitTestCardObject(10, 10) };

            int eventTriggers = 0;
            playerHandContainer.CardLayoutsChangedEvent += delegate (object sender, CardLayoutChangedEventArgs e)
            {
                eventTriggers += 1;
                e.Card.SetLayout(e.CardLayout);
            };
            CardLayout layout = cardContainer.AddCard(cards[0]);

            Assert.AreEqual(1, playerHandContainer.Hand.Count);
            Assert.AreEqual(new Vector2(35, 10), layout.Position);
            Assert.AreEqual(0, eventTriggers);

            cardContainer.AddCard(cards[1]);

            Assert.AreEqual(2, playerHandContainer.Hand.Count);
            Assert.AreEqual(new Vector2(30, 10), playerHandContainer.Hand[0].cardLayout.Position);
            Assert.AreEqual(new Vector2(40, 10), playerHandContainer.Hand[1].cardLayout.Position);
            Assert.AreEqual(1, eventTriggers);
            eventTriggers = 0;
            Assert.AreEqual(new Vector3(30, 10, 0), playerHandContainer.Hand[0].card.Transforms.Position);
            //the caller is responsible for applying the layout
            Assert.AreEqual(new Vector3(0, 0, 0), playerHandContainer.Hand[1].card.Transforms.Position);

            layout = cardContainer.AddCard(cards[2]);
            Assert.AreEqual(3, playerHandContainer.Hand.Count);
            Assert.AreEqual(new Vector2(25, 10), playerHandContainer.Hand[0].cardLayout.Position);
            Assert.AreEqual(new Vector2(35, 10), playerHandContainer.Hand[1].cardLayout.Position);
            Assert.AreEqual(new Vector2(45, 10), playerHandContainer.Hand[2].cardLayout.Position);
            Assert.AreEqual(2, eventTriggers);
            eventTriggers = 0;
            Assert.AreEqual(new Vector3(25, 10, 0), playerHandContainer.Hand[0].card.Transforms.Position);
            Assert.AreEqual(new Vector3(35, 10, 0), playerHandContainer.Hand[1].card.Transforms.Position);
            //the caller is responsible for applying the layout
            Assert.AreEqual(new Vector3(0, 0, 0), playerHandContainer.Hand[2].card.Transforms.Position);

            //Test to see if the card container is calling back to the CardObject to check the bounds
            cards[2].SetLayout(layout);
            Assert.AreSame(cards[0], cardContainer.CardAtPoint(30, 15));
            Assert.AreSame(cards[1], cardContainer.CardAtPoint(40, 15));
            Assert.AreSame(cards[2], cardContainer.CardAtPoint(50, 15));
            Assert.IsNull(cardContainer.CardAtPoint(60, 15));

            cardContainer.RemoveCard(cards[0]);

            Assert.AreEqual(2, playerHandContainer.Hand.Count);
            Assert.IsFalse(playerHandContainer.ContainsCard(cards[0]));
            Assert.IsTrue(playerHandContainer.ContainsCard(cards[1]));
            Assert.IsTrue(playerHandContainer.ContainsCard(cards[2]));
            Assert.AreEqual(2, eventTriggers);
            Assert.AreEqual(new Vector2(30, 10), playerHandContainer.Hand[0].cardLayout.Position);
            Assert.AreEqual(new Vector2(40, 10), playerHandContainer.Hand[1].cardLayout.Position);
            Assert.AreEqual(new Vector3(30, 10, 0), playerHandContainer.Hand[0].card.Transforms.Position);
            Assert.AreEqual(new Vector3(40, 10, 0), playerHandContainer.Hand[1].card.Transforms.Position);

        }
        [TestMethod]
        public void PlayerDeckContainerTest()
        {
            PlayerDeckContainer playerDeckContainer = new PlayerDeckContainer();

            //Test Default values
            Assert.AreEqual(new CardLayout(), playerDeckContainer.Layout);
            Assert.AreEqual(0, playerDeckContainer.Deck.Count);

            //Test Constructor
            CardLayout cardLayout = new CardLayout() { Position = new Vector2(20, 20), Rotation = (float)(Math.PI / 2), Scale = 1 };
            playerDeckContainer = new PlayerDeckContainer(cardLayout);

            Assert.AreEqual(cardLayout, playerDeckContainer.Layout);
            Assert.AreEqual(0, playerDeckContainer.Deck.Count);

            CardObject[] cards = new CardObject[] { new UnitTestCardObject(10, 10), new UnitTestCardObject(10, 10), new UnitTestCardObject(10, 10) };
            playerDeckContainer.Deck.Add(cards[0]);
            playerDeckContainer.Deck.Add(cards[1]);

            //We should see stack behavior
            Assert.AreSame(cards[1], playerDeckContainer.GetTopCard());

            //Test Card Container implementation
            ICardContainer cardContainer = playerDeckContainer;
            //Deck Card container behaves like normal List. Basically it's just a list of card objects that
            //all share the same Card Layout
            CardLayout cardLayout2 = cardContainer.AddCard(cards[2]);

            Assert.AreSame(cards[2], playerDeckContainer.GetTopCard());
            Assert.AreEqual(cardLayout, cardLayout2);

            cards[0].SetLayout(cardLayout);
            cards[1].SetLayout(cardLayout);
            cards[2].SetLayout(cardLayout);

            Assert.AreSame(playerDeckContainer.GetTopCard(), cardContainer.CardAtPoint(15, 25));
            Assert.IsNull(cardContainer.CardAtPoint(25, 15));

            cardContainer.RemoveCard(cards[2]);

            Assert.AreSame(cards[1], playerDeckContainer.GetTopCard());

            cardContainer.RemoveCard(cards[0]);

            Assert.AreSame(cards[1], playerDeckContainer.GetTopCard());
            Assert.IsFalse(cardContainer.ContainsCard(cards[0]));

            cardContainer.RemoveCard(cards[1]);

            Assert.AreSame(null, playerDeckContainer.GetTopCard());

        }
        [TestMethod]
        public void CascadeContainerTest()
        {
            //Test default values
            CascadeContainer cascadeContainer =  new CascadeContainer();

            Assert.AreEqual(new Vector2(), cascadeContainer.Position);
            Assert.AreEqual(0, cascadeContainer.Rotation);
            Assert.AreEqual(1, cascadeContainer.Scale);

            //Test Constructor
            int cardWidth = 10;
            cascadeContainer = new CascadeContainer(50, 40, (float)(Math.PI/2), 2, cardWidth, true, 5);

            Assert.AreEqual(new Vector2(50, 40), cascadeContainer.Position);
            Assert.AreEqual((float)(Math.PI / 2), cascadeContainer.Rotation);
            Assert.AreEqual(2, cascadeContainer.Scale);

            //Test interface implementation
            CardObject[] cards = { new UnitTestCardObject(cardWidth, cardWidth),
                new UnitTestCardObject(cardWidth, cardWidth), new UnitTestCardObject(cardWidth, cardWidth) }; 
            ICardContainer container = cascadeContainer;
            CardLayout layout = container.AddCard(cards[0]);
            cards[0].SetLayout(layout);

            Assert.AreEqual(new Vector2(50, 40), layout.Position);
            Assert.AreEqual((float)(Math.PI / 2), layout.Rotation);
            Assert.AreEqual(2, layout.Scale);

            //Checking the second card added
            layout = container.AddCard(cards[1]);
            cards[1].SetLayout(layout);

            //Remeber to account for the offset of 5
            Assert.AreEqual(new Vector2(50, 55), layout.Position);
            Assert.AreEqual((float)(Math.PI / 2), layout.Rotation);
            Assert.AreEqual(2, layout.Scale);

            //Checking the third card added
            layout = container.AddCard(cards[2]);
            cards[2].SetLayout(layout);

            Assert.AreEqual(new Vector2(50, 70), layout.Position);
            Assert.AreEqual((float)(Math.PI / 2), layout.Rotation);
            Assert.AreEqual(2, layout.Scale);

            //Test removing cards
            int eventTriggers = 0;
            cascadeContainer.CardLayoutsChangedEvent += delegate (object sender, CardLayoutChangedEventArgs e)
            {
                eventTriggers += 1;
                e.Card.SetLayout(e.CardLayout);
            };

            container.RemoveCard(cards[0]);

            Assert.AreEqual(2, eventTriggers);
            Assert.IsFalse(container.ContainsCard(cards[0]));
            Assert.AreEqual((float)(Math.PI / 2), cards[1].Rotation);
            Assert.AreEqual(2, cards[1].Scale);
            Assert.AreEqual(new Vector3(50, 40, 0), cards[1].Transforms.Position);
            //Test Card picking
            Assert.IsNull(container.CardAtPoint(0, 0));
            Assert.AreSame(cards[1], container.CardAtPoint(40, 50));
            Assert.AreSame(cards[2], container.CardAtPoint(40, 57));
            Assert.IsNull(container.CardAtPoint(40, 90));

        }
    }
    public class UnitTestCardObject : CardObject
    {
        private int width;
        private int height;
        public UnitTestCardObject()
        { }
        public UnitTestCardObject(int width, int height)
        {
            this.width = width;
            this.height = height;
        }
        public override float Width
        {
            get { return width; }
        }
        public override float Height
        {
            get { return height; }
        }
    }
}
